const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let schema_db = new Schema({
    doi: {
        type: String 
    },
    cuenta: {
        type: String 
    },
    tipo: {
        type: String 
    }, 
    options: {
        type: JSON 
    },
    date: {
        type: String ,
        default: new Date() 
    } 
});
 
schema_db.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
schema_db.plugin(autoIncrement.plugin, 'MovOptionsDataAutoincrement');
module.exports = mongoose.model('MovOptions', schema_db);