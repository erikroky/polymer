const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let movimientoSchema = new Schema({
    cuenta: {
        type: String 
    },
    cuenta_abono: {
        type: String 
    },
    
    doi_abono: {
        type: String 
    },
    canal: {
        type: String 
    },
    operacion: {
        type: String 
    },
    doi: {
        type: String 
    },
    monto: {
        type: String 
    },
    referencia: {
        type: String 
    },
    fechahora: {
        type: String 
    },
    tipopago: {
        type: String 
    }
});
 
movimientoSchema.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
movimientoSchema.plugin(autoIncrement.plugin, 'Book');
module.exports = mongoose.model('Movimiento', movimientoSchema);