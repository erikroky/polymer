const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


let rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol válido'
};


let Schema = mongoose.Schema;


let usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, ' es necesario']
    },
    email: {
        type: String,
        unique: true,
        required: [true, ' es necesario']
    },
    doi: {
        type: String,
        unique: true,
        required: [true, ' es necesario'] 
    },
    nacimiento: {
        type: String, 
        required: [true, ' es necesario']
    },
    tel1: {
        type: String, 
        required: [false]
    },
    tel2: {
        type: String, 
        required: [false]
    },
    tel3: {
        type: String, 
        required: [false]
    },
    direccion: {
        type: String, 
        required: [true, ' es necesario']
    },
    password: {
        type: String,
        required: [true, ' es obligatoria']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }
});


usuarioSchema.methods.toJSON = function() {

    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
}


usuarioSchema.plugin(uniqueValidator, { message: ' debe de ser único' });


module.exports = mongoose.model('Usuario', usuarioSchema);