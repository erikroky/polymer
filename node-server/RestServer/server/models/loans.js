const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let loansSchema = new Schema({
    cuenta: {
        type: String 
    },
    tipomoneda: {
        type: String 
    }, 
    monto: {
        type: String 
    }, 
    adicionales: {
        type: Boolean,
        default: false
    },
    meses: {
        type: Number
    },
    tasa: {
        type: String  
    },
    fechahora: {
        type: String ,
        default: new Date() 
    }
});
 
loansSchema.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
loansSchema.plugin(autoIncrement.plugin, 'LoansAutoincrement');
module.exports = mongoose.model('Loans', loansSchema);