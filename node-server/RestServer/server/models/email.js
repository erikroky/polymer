const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let schema_db = new Schema({
    doi: {
        type: String 
    },
    email: {
        type: String 
    }, 
    default: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: false
    },
    gui:{
        type:String,
        default: false
    }
});
 
schema_db.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
schema_db.plugin(autoIncrement.plugin, 'EmailDataAutoincrement');
module.exports = mongoose.model('Email', schema_db);