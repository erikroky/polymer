const mongoose = require('mongoose'),
autoIncrement = require('mongoose-auto-increment');
let Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

let schema_db = new Schema({
    doi: {
        type: String 
    },
    opt: {
        type: String 
    }, 
    tipo: {
        type: String 
    },
    cuenta: {
        type: String,
        default: ""
    }, 
    opt1: {
        type: Boolean,
        default: false
    },
    opt2: {
        type: Boolean,
        default: false
    },
    mail: {
        type: String 
    }
});
 
schema_db.methods.toJSON = function() { 
    let movim = this;
    let movimObject = movim.toObject(); 
    return movimObject;
}

 
schema_db.plugin(autoIncrement.plugin, 'OptionsDataAutoincrement');
module.exports = mongoose.model('Option', schema_db);