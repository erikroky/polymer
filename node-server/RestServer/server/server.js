require('./config/config');

const express = require('express');
const mongoose = require('mongoose');
const http = require('http');

const socketIO= require('socket.io');
const app = express();
let server = http.createServer(app);
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

module.exports.io = socketIO(server);
// Configuración global de rutas
app.use(require('./routes/index'));
require('./routes/settings');


mongoose.connect(process.env.URLDB, { connectTimeoutMS: 5000 }, (err, res) => {
    if (err) {

        throw err
    } else {
        console.log(`Base de datos Online`);
    }
    //console.log({ res });
});


server.listen(process.env.PORT,(err) => {
    if (err) throw new Error(err);
    console.log('Escuchando puerto: ', process.env.PORT);
});