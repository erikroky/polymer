// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000; 

// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


// ============================
//  Vencimiento del Token
// ============================
// 60 segundos
// 60 minutos
// 24 horas
// 30 días
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;


// ============================
//  SEED de autenticación
// ============================
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo';

// ============================
//  Base de datos
// ============================
let urlDB,portHOST,urlHOST;

if (process.env.NODE_ENV === 'dev') { 
    urlDB = 'mongodb://admin1:admin1@ds213199.mlab.com:13199/usuarios';
    portHOST= process.env.PORT
    urlHOST='http://127.0.0.1:'+portHOST;
}else if (process.env.NODE_ENV === 'test') { 
    urlDB = 'mongodb://admin1:admin1@ds213199.mlab.com:13199/usuarios';
    portHOST= 8081
    urlHOST='http://node0.latam-server.com:'+portHOST;
}  else {
    urlDB = process.env.MONGO_URI;
}
process.env.URLDB = urlDB;
process.env.PORT  = portHOST;
process.env.URLHOST  = urlHOST;