const express = require('express');
//import validator as EmailValidator from 'email-validator';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken'); 
const uuid = require('node-uuid');
const EmailModel = require('../models/email'); 
const OptionModel = require('../models/option'); 
const OptionModelMov = require('../models/movopciones'); 
const mailvalidator = require("email-validator");
const nodemailer = require('nodemailer');
const Catalogo = require('../models/catalogo');
var sortJsonArray = require('sort-json-array');
const { io } = require('../server');
const app = express();
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    
    next();
 });
 io.on('connection', (client) => {

    console.log('Usuario conectado');

     

});
app.post('/email', (req, res) => {
    console.info(req.body); 
    let body = req.body; 

     
    if (body.email.length<1||body.email==null || body.email== undefined){return res.status(400).json({ok: false, err: { message:  "el email es requerido"}});    }
    if ( mailvalidator.validate(body.email)==false){return res.status(400).json({ok: false, err: { message:  "el formato de correo es incorrecto"}});    }

    
    if (body.doi==null || body.doi== undefined){return res.status(400).json({ok: false, err: { message:  "el doi es requerido"}});    }

    EmailModel.findOne({ email: body.email }, (err, emailResp) => { 
        if (err) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Error Inesperado, en la base de datos'
                } 
            });
        } 
        console.info(emailResp);
        if (emailResp == null || emailResp== undefined) {
            
            console.info('Registrar');
            registrarEmail(body,res);

      
            return res.status(200).json({
               ok: true,
               message: "Informacion Registrada"
          }); 
           
        } 

        if (emailResp != null ) {
            return res.status(400).json({
                ok: true,
                err: {
                    message: 'Operacio cancelada, Correo electrónico ya existe'
                } 
            }); 
        }
    }); 
}); 

 
app.get('/email/:doi', (req, res) => {
    console.info(req.body); 
    let doi = req.params.doi; 
    //let body = req.body; 
 
    if (doi==null || doi== undefined){return res.status(400).json({ok: false, err: { message:  "el doi es requerido"}});    }

    //EmailModel.find({ email: body.doi }, (err, emailResp) => { 
        EmailModel.find({  doi: doi,enable: true },   { email: 1, _id: 0 })
        .exec((err, emailResp) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Error Inesperado, en la base de datos'
                }
            });
        } 
        if (emailResp != null ) {
            res.json({
                ok: true,
                emails: emailResp
            }); 
        }
    }); 
}); 


 
app.get('/verifyemail/:guid', (req, res) => {
    console.info(req.params); 
    let guid = req.params.guid;   
    EmailModel.findOneAndUpdate({ gui: guid,enable: false}, {enable: true}, (err, opcionupdated) => {         
        if (err) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'No existe la informacion'
                }
            });
        } 
       
        if (opcionupdated != null ) {
            io.sockets.emit('validatemail', {
                usuario: "este email: "+opcionupdated.mail+': esta verificado'
            });
            res.send( '<center><h2>Correo electrónico verificado</h2></center>');
        }
        if (opcionupdated == null ) {
            res.send( '<center> <h2>No existe Informacion</h2></center>');
        }
    }); 
}); 


app.get('/obteneropciones/:padre', (req, res) => {
    console.info(req.params); 
    let guid = req.params.padre;   
    Catalogo.findOneAndUpdate({ subproductos: guid}, (err, opcionupdated) => {         
        if (err) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'No existe la informacion'
                }
            });
        } 
       
        if (opcionupdated != null ) {
            return res.status(500).json({
                ok: false,
                data:  opcionupdated
            });
        }
        if (opcionupdated == null ) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'no existe informacion'
                }
            });
        }
    }); 
}); 

 
/**
 * doi: parametro de busqueda
 * codigo: Parametro de busqueda
 * opcion: Parametro de validacion, para ver si va a la opcion 1 u opcion 2
 * valoropcion: Valor del check 
 */
app.put('/optionsv1', (req, res) => {
    console.info(req.body);
   let body=req.body;

    if (body.doi==null || body.doi== undefined){return res.status(400).json({ok: false, err: { message:  'el doi es requerido'}});}
    if (body.codigo==null || body.codigo== undefined){return res.status(400).json({ok: false, err: { message:  'el codigo es requerido'}});}
    if (body.opcion==null || body.opcion== undefined){return res.status(400).json({ok: false, err: { message:  'opcion de formato es requerido'}});}
    if (body.tipo==null || body.tipo== undefined){return res.status(400).json({ok: false, err: { message:  'El tipo de producto es requerido'}});}
    if (body.cuenta.length<1 ||body.cuenta==null || body.cuenta== undefined){return res.status(400).json({ok: false, err: { message:  'La cuenta es requerida'}});}

    let doi = req.body.doi;
    let codigo = req.body.codigo; 
    let valoropcion = req.body.valoropcion;
    let tipos = req.body.tipo; 
    let cuenta= req.body.cuenta;

    if(req.body.opcion==='1'){
        req.body.opcion="opt1";
    }else if(req.body.opcion==='2'){
        req.body.opcion="opt2";
    } 
    let opcion = req.body.opcion;

    //ACTUALIZAR OPCIONES
    
    let sets=null;
    if(req.body.opcion==='opt2' && valoropcion==false ){
        console.info("cambia corre value");
        sets={ [opcion]: valoropcion, mail: ""}
    }else {
        console.info("no cambia corre value");
        sets={ [opcion]: valoropcion}
    }

    console.info(req.body);
    OptionModel.findOneAndUpdate({ doi: doi,opt: codigo,tipo: tipos,cuenta: cuenta},sets, (err, opcionupdated) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err: { message:  'Error de base de datos'}});
        } 
        if (opcionupdated==null) {
            return res.status(400).json({
                ok: false,
                err: { message:  'No existe info, para actualizar'}
            });
        } 
        console.info('usuario actualizado');
        return res.status(200).json({
            ok: true,
            update: opcionupdated
        });
    });

}); 
 

function _getOptions(body, callback) {
     
   
}

app.post('/finalcuenta', (req, res) => {
    console.info(req.body);
   let body=req.body;

    if (body.doi==null || body.doi== undefined){return res.status(400).json({ok: false, err: { message:  'el doi es requerido'}});}
    if (body.cuenta.length<1 ||body.cuenta==null || body.cuenta== undefined){return res.status(400).json({ok: false, err: { message:  'el cuenta es requerido'}});}

    let doi = req.body.doi; 
    let cuenta= req.body.cuenta;

    OptionModel.find({  doi: body.doi,cuenta: body.cuenta},   {  doi:1,  opt:1,  tipo:1, cuenta:1,    opt1:1,  opt2:1,   mail:1,_id: 0 })
    .exec((err, opcioneslista) => { 
        //callback(emailResp);
        if (err) {
            return res.status(400).json({
                ok: false,
                err:  { message:  'Erro inesperado 0'},
                deta: err
            });
        }

        let failmail=0
        opcioneslista.forEach((element, index) => { 
            if(element.opt2==true && element.mail.length<1){
                failmail=1;
            }
        });
        if(failmail==1){
            return res.status(400).json({
                ok: false,
                err: { 
                    message: "Operacion Cancelada, selecione correo"
                }
            });
        }
       

    let mov = new OptionModelMov({ 
        doi: doi, 
        cuenta: cuenta,
        options:opcioneslista
    });

    mov.save((err, newrow) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err:  { message:  'Erro inexperado 1'},
                deta: err
            });
        }

        res.json({
            ok: true,
            movement: newrow
        });


    });
        
    });
 
   

}); 

app.put('/optionschangemail', (req, res) => {
    let body = req.body;
    console.info("bodyy change mailllll");
    console.info(body);
    if (body.doi==null || body.doi== undefined){return res.status(400).json({ok: false, err: { message:  'el doi es requerido'}});}
    if (body.codigo==null || body.codigo== undefined){return res.status(400).json({ok: false, err: { message:  'el codigo es requerido'}});}
    //if (body.valoropcion==null || body.valoropcion== undefined){return res.status(400).json({ok: false, err: { message:  'el valoropcion es requerido'}});}
    if (body.tipo==null || body.tipo== undefined){return res.status(400).json({ok: false, err: { message:  'el tipo es requerido'}});}
    if (body.cuenta.length<1 ||body.cuenta==null || body.cuenta== undefined){return res.status(400).json({ok: false, err: { message:  'el cuenta es requerido'}});}


    console.info(req.body);
    let doi = req.body.doi;
    let codigo = req.body.codigo; 
    let email = req.body.email;
    let tipos = req.body.tipo; 
    let  cuenta= req.body.cuenta;
    console.info(req.body);
 
    OptionModel.findOneAndUpdate({ doi: doi,opt: codigo,tipo: tipos,cuenta: cuenta,opt2: true}, { mail: email}, (err, opcionupdated) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err: { message:  'No se puede actualizar'}
            });
        } 

        if(opcionupdated==null && body.email.length>0){
            return res.status(400).json({
                ok: false,
                err: { message:  'Operacion Cancelada, Primero seleccione la opcion de envío digital e intente nuevamente'}
            });
        }
        console.info('usuario actualizado');
        return res.status(200).json({
            ok: true,
            update: opcionupdated
        });
    });

}); 
function _getEmailsEnabled(st, callback) {
    EmailModel.find({  doi: st,  enable: true },   { email: 1, _id: 0 })
    .exec((err, emailResp) => {
        callback(emailResp);
    });
}

app.get('/options/:doi/:tipo', (req, res) => {
    console.info(req.params); 
     
    let doi   = req.params.doi; 
    let tipos = req.params.tipo; 

    let valu=[]; 
    _getEmailsEnabled(doi, function (res) {
        //console.log(res); 
        valu = res; 
    });
    console.info("doi:"+doi+"tipo:"+tipos)
    OptionModel.find({ doi: doi, tipo: tipos})
        .exec((err, options) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            let c=0, notfound=0; var lista=[];
           
            
           
            options.forEach((element, index) => { 

                Catalogo.findOne({ codigo: element.opt }, 'nombre') 
                .exec((err, catalogOut) => { 
                    if(catalogOut!=null){ 
                        
                       
                        console.log(valu); 
                        var newobj = { 
                            nombre: catalogOut.nombre, 
                            opt:options[index].opt,
                            opt1:options[index].opt1,
                            opt2:options[index].opt2,
                            mail:options[index].mail,
                            mails:valu
                          } 
                        lista.push(newobj);
                        c=c+1;
                    }else {
                        notfound=notfound+1;
                    }
                  
                    if(notfound+c==options.length){  
                        notfound=0;
                        res.status(200).json({
                            ok: true,
                            next: false,
                            options: sortJsonArray(lista, 'opt')
                        });
                        
                    }
                });  
                
            });

           console.info(lista); 
    

        });

}); 

app.get('/optionsv1/:doi/:tipo/:cuenta', (req, res) => {
    console.info(req.params); 
     
    let doi   = req.params.doi; 
    let tipos = req.params.tipo; 
    let cuenta = req.params.cuenta; 
        if(cuenta=='EMPTY'){
            return res.status(200).json({
                ok: true,
                next: false,
                err: { message:  'Solicitud vacia'}
            }); 
        }
    let valu=[]; 
    _getEmailsEnabled(doi, function (res) {
        //console.log(res); 
        valu = res; 
    });
    console.info("doi:"+doi+"tipo:"+tipos)
    OptionModel.find({ doi: doi, tipo: tipos, cuenta: cuenta})
        .exec((err, options) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            console.info('opciones');
            console.info('===============');
            console.info(options);
            if (options.length<1) {
                return res.status(200).json({
                    ok: true,
                    next: true,
                    err: { message:  'No existe con CUENTA'}
                });
            }

            let c=0, notfound=0; var lista=[];
           
            
           
            options.forEach((element, index) => { 

                Catalogo.findOne({ codigo: element.opt }, 'nombre') 
                .exec((err, catalogOut) => { 
                    if(catalogOut!=null){ 
                        
                       
                        console.log(valu); 
                        var newobj = { 
                            nombre: catalogOut.nombre, 
                            opt:options[index].opt,
                            opt1:options[index].opt1,
                            opt2:options[index].opt2,
                            mail:options[index].mail,
                            mails:valu
                          } 
                        lista.push(newobj);
                        c=c+1;
                    }else {
                        notfound=notfound+1;
                    }
                  
                    if(notfound+c==options.length){  
                        notfound=0;
                        return res.status(200).json({
                            ok: true,
                            next: false,
                            options: sortJsonArray(lista, 'opt')
                        });
                        
                    }
                });  
                
            });

           console.info(lista); 
    

        });

}); 

var sort_by = function(field, reverse, primer){

    var key = primer ? 
        function(x) {return primer(x[field])} : 
        function(x) {return x[field]};
 
    reverse = !reverse ? 1 : -1;
 
    return function (a, b) {
        return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
      } 
 }
 
/**
 * Registrar Opciones
 */

app.put('/options', (req, res) => {
   // console.info(req.body); 
    let body = req.body; 
 
    OptionModel.findOne({ doi: body.doi,tipo: body.tipo,cuenta: body.cuenta  }, (err, respdb) => { 
        if (err) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: 'Error Inesperado, en la base de datos'
                }
            });
        } 
        if (respdb == null || respdb== undefined) {
            
            console.info('Registrar');
            body.opciones.forEach(function(val,index){ 
                console.log(index+': '+val.opt) ;
                console.log(index+': '+val.opt) ;
                registrarOpciones(body.doi,body.tipo,body.cuenta,val.opt,val.opt1,val.opt2,val.mail,res);
             })  

      
         return res.status(200).json({
               ok: true,
               message: "Informacion Registrada"
          }); 
           
        } 

        if (respdb != null ) { 
            console.info('Actualizar'); 
            body.opciones.forEach((item) => {
                OptionModel.update({"doi": body.doi, "opt":item.opt, "tipo":item.tipo,cuenta: body.cuenta}, { }, {upsert: false}, (err, task) => {
                        if (err) {
                          console.log(err);
                        }
                });
             }); 
            res.json({
                ok: true,
                message: "La informacion, Informacion actualizada"
            }); 
        }
        
    }); 
}); 





/**
 * Funcion para registrar opcions de 
 */
registrarOpciones=(doi,tip,cuenta,option, opt1,opt2,email,res)=> { 
    let obj = new OptionModel({
        doi:  doi ,
        tipo:  tip ,
        cuenta: cuenta,
        opt: option, 
        opt1:  opt1 ,
        opt2: opt2,
        mail: email
    });

    obj.save((err, modeStatus) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }  
    });
 
}
 
/**
 * Funcion para registrar una direccion de corroe electronico
 */
registrarEmail= (body,res)=> {
    var uid=uuid.v4()+uuid.v4()+uuid.v4()+uuid.v4()+uuid.v4();
    let obj = new EmailModel({
        doi: body.doi,
        email:body.email,
        default:true,
        enable:false,
        gui: uid
    });

    obj.save((err, modeStatus) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }  
    });

    var link_= process.env.URLHOST+'/verifyemail/'+uid; 
    SendMail(body.email,link_);
}



SendMail = (email,link) => { 
var transporter = nodemailer.createTransport({host: "hd-4918.banahosting.com",  port: 465,  secure: true,   auth: {  user: "api-test@latam-server.com",  pass: "bbva2019!"  },logger: true,debug: true   });
      
      var body='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" /><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style type="text/css">ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%}body{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0 !important}p{margin:1em 0}table td{border-collapse:collapse}img{outline:0}a img{border:none}@-ms-viewport{width:device-width}</style><style type="text/css">@media only screen and (max-width: 480px){.container{width:100% !important}.footer{width:auto !important;margin-left:0}.mobile-hidden{display:none !important}.logo{display:block !important;padding:0 !important}img{max-width:100% !important;height:auto !important;max-height:auto !important}.header img{max-width:100% !important;height:auto !important;max-height:auto !important}.photo img{width:100% !important;max-width:100% !important;height:auto !important}.drop{display:block !important;width:100% !important;float:left;clear:both}.footerlogo{display:block !important;width:100% !important;padding-top:15px;float:left;clear:both}.nav4,.nav5,.nav6{display:none !important}.tableBlock{width:100% !important}.responsive-td{width:100% !important;display:block !important;padding:0 !important}.fluid,.fluid-centered{width:100% !important;max-width:100% !important;height:auto !important;margin-left:auto !important;margin-right:auto !important}.fluid-centered{margin-left:auto !important;margin-right:auto !important}body{padding:0px !important;font-size:16px !important;line-height:150% !important}h1{font-size:22px !important;line-height:normal !important}h2{font-size:20px !important;line-height:normal !important}h3{font-size:18px !important;line-height:normal !important}.buttonstyles{font-family:arial,helvetica,sans-serif !important;font-size:16px !important;color:#FFF !important;padding:10px !important}}@media only screen and (max-width: 640px){.container{width:100% !important}.mobile-hidden{display:none !important}.logo{display:block !important;padding:0 !important}.photo img{width:100% !important;height:auto !important}.nav5,.nav6{display:none !important}.fluid,.fluid-centered{width:100% !important;max-width:100% !important;height:auto !important;margin-left:auto !important;margin-right:auto !important}.fluid-centered{margin-left:auto !important;margin-right:auto !important}}</style><!--[if mso]><style type="text/css">body,table,td{font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#000;line-height:1}</style><![endif]--></head><body bgcolor="#ffffff" text="#000000" style="background-color: #ffffff; color: #000000; padding: 0px; -webkit-text-size-adjust:none; font-size: 16px; font-family:arial,helvetica,sans-serif;"><div style="font-size:0; line-height:0;"><img src="https://click.mailing.bbvaapimarket.com/open.aspx?ffcb10-fec8127475660c7e-fe1b1678736204797c1c79-fe8e137375670c797d-ff3117747462-fe26127373600674711172-ff61177472" width="1" height="1"></div><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"><tr><td align="center" valign="top"></td></tr><tr><td align="center"><table cellspacing="0" cellpadding="0" border="0" width="600" class="container" align="center"><tr><td><table class="tb_properties border_style" style="background-color:#FFFFFF;" cellspacing="0" cellpadding="0" bgcolor="#ffffff" width="100%"><tr><td align="center" valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td class="content_padding" style=""><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" class="header" valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="left" valign="top"><table cellspacing="0" cellpadding="0" style="width:100%"><tbody><tr><td class="responsive-td" valign="top" style="width: 100%;"><table cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%; " class="stylingblock-content-wrapper"><tr><td class="stylingblock-content-wrapper camarker-inner"> <img src="https://click.mailing.bbvaapimarket.com/open.aspx?ffcb10-fec8127475660c7e-fe1b1678736204797c1c79-fe8e137375670c797d-ff3117747462-fe26127373600674711172-ff61177472" width="1" height="1"><div style="max-width:600px"><div id="header" style="width: 600px; height: 74px;background-color: #072146;padding:20px 38px 20px 38px;box-sizing:border-box"> <img style="width: 200px;height: 34px;object-fit: contain;" src="https://www.bbvanexttechnologies.com/wp-content/uploads/2018/06/logo-BBVA-Next-Technologies.png"></div><div id="welcomeContent" style="padding: 48px 48px 48px 48px;text-align: center"><div style="margin-bottom:14px"> <img style="margin:0 auto 32px auto" src="https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/welcome-mail-1.png" srcset="https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/welcome-mail-1%402x.png 2x, https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/welcome-mail-1%403x.png 3x" class="img-2"><p style="max-width: 448px;height:20px;font-family: Arial;font-size: 20px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1em;letter-spacing: normal;text-align: center;color: #121212;margin: 0 auto 16px auto;">Registro de usuario en Zona BBVA</p><p style="font-family: Arial;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.43;letter-spacing: normal;text-align: center;color: #666666;margin: 16px auto 16px auto;">Estás recibiendo este email porque estas agregando  @EMAIL@ a tu lista de correos electrónicos, en nuestra plataforma online. Para activar el correo electronico, por favor, haz click en el siguiente enlace.</p> <a target="_blank" href="@LINK@" style="text-decoration:none;cursor:pointer;font-family: Arial;font-size: 15px;font-weight: bold;font-style: normal;font-stretch: normal;line-height: 1.6;letter-spacing: normal;text-align: left;color: #1973b8;"> <img style="width:16px;height:16px;vertical-align:middle;" src="https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/plus.png" class="Health"> Activar Cuenta</a><p style="font-family: Arial;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.43;letter-spacing: normal;text-align: center;color: #666666;margin: 16px auto 16px auto;"> si no puede visualizar el enlace por favor copia en tu navegador: <br>@LINK1@</p></div><div style="margin-bottom:14px"><p style="max-width: 448px;font-family: Arial;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.43;letter-spacing: normal;text-align: center;color: #666666;margin: 0px auto 16px auto;">Si crees que este correo te ha llegado por error, por favor, consulta con nosotros. Muchas gracias</p><p style="max-width: 448px;font-family: Arial;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.43;letter-spacing: normal;text-align: left;color: #666666;margin: 0px auto 16px auto;"></p></div></div><div class="footer" style="background-color: #072146;color:white;padding:25px 48px 25px 48px;"> <img style="margin:0 0 12px 0" src="https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/logo-api-market-small.png" srcset="https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/logo-api-market-small%402x.png 2x, https://s3-eu-west-1.amazonaws.com/static.bbvaapimarket.com/img/mail/logo-api-market-small%403x.png 3x" class="img-2"><div id="social" style="margin-bottom: 31px"> <a href="https://click.mailing.bbvaapimarket.com/?qs=1eacbf92069833a1adfba858543f18ff52d7724472d1f086d58df7af35ef408f3a8bcc76817222da581776164d875f277a12c5c40387e81b" target="_blank" style="text-decoration: none; cursor: pointer"> <img style="height: 24px; width: 24px; margin-right: 15px" class="twitter-filled" src="https://ci5.googleusercontent.com/proxy/z1_RB-gEcglHTMP36sj1k56NDinHADZGw7fCGu9xIuupmpnmW8qwmj79E5jrchJju27m14e6X01OtGR4sEB-kwsiK4MHF5A24IMZWBdLiY3eJ2hYULjGruBRXwfB=s0-d-e1-ft#https://s3-eu-west-1.amazonaws.com/bbva-api-market/img/mail/Desktop.png"> </a> <a href="https://click.mailing.bbvaapimarket.com/?qs=b794c04f935663a358a715f18c0e09a4e2c9fcd44e62d6ae0fecd4c3a065bf4f379d2e63039e64957590252710221dbd43db96851cae1baf" target="_blank" style="text-decoration: none; cursor: pointer"> <img src="https://ci4.googleusercontent.com/proxy/D_3wTOdGa57YnqWxtNyMfYrVrAMq7iCRx02kkgl7nBky9Rd-cyKv9mxOEiOb6gWynqj1oKiwcrBzYyZzrRf2YyL__5w3hgtsPG7Yz2yqoksUwMK5S7dR5KYwAHJmEG3LzXQdpQ=s0-d-e1-ft#https://s3-eu-west-1.amazonaws.com/bbva-api-market/img/mail/Twitter-Filled.png"> </a></div><p style="font-family: Arial;font-size: 10px;font-weight: normal;font-style: normal;font-stretch: normal;line-height: 1.35em;letter-spacing: normal;text-align: left;color: #ffffff;"> Este mensaje ha sido enviado por un sistema automático. No es preciso responder directamente a este email. Por motivos de seguridad, se recomienda comprobar siempre la dirección que aparece en la barra de navegación. Este mensaje, incluido cualquier archivo adjunto, pertenece de manera exclusiva al ámbito de la comunicación privada entre BBVA y el destinatario, pudiendo contener información confidencial y sujeta al secreto bancario. En caso de haber recibido este mensaje por error por favor comunicarlo a través del email: <b><a href="mailto:api.market.support@bbva.com" target="_blank" style="text-decoration:none;font-weight:bold;color:white">api.market.support@bbva.com</a></b>. Asimismo, informamos de que la distribución, copia o utilización de este mensaje, o de cualquier archivo adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley. Si Usted no desea seguir recibiendo comunicaciones comerciales o publicitarias a través de este canal, por favor solicítelo a través de este buzón: <b><a style="text-decoration:none;font-weight:bold;color:white" href="mailto:api.market.support@bbva.com" target="_blank">api.market.support@bbva.com</a></b>.</p></div></div> <img src="https://click.mailing.bbvaapimarket.com/open.aspx?ffcb10-fec8127475660c7e-fe1b1678736204797c1c79-fe8e137375670c797d-ff3117747462-fe26127373600674711172-ff61177472" width="1" height="1"></custom></custom></td></tr></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr><tr><td valign="top"></td></tr></table></body></html>';
      body=body.replace('@EMAIL@',email);
      body=body.replace('@LINK@',link); 
      body=body.replace('@LINK1@',link);
      var mailOptions = {
          from: '"BBA API " <api-test@latam-server.com>', // sender address
          to: email, // list of receivers
          subject: 'ZONA BBVA. Activar correo electrónico ', // Subject line
          //text: 'Hello world?', // plain text body
          html: body // html body
      };

      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        }
      });
}


module.exports = app;