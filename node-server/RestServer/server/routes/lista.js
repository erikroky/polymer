const express = require('express'); 
const _ = require('underscore');

const Catalogo = require('../models/catalogo');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

const app = express();
app.all('*', function(req, res, next) {
    //res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
 });
  
app.get('/lista/:tipolista',verificaToken,(req, res) => {
 
let _lista = req.params.tipolista;
 
   /* let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);
*/
    Catalogo.find({ lista: _lista },  {  _id: 0 })
     /*   .skip(desde)
        .limit(limite)*/
        .exec((err, catalogos) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                catalogos: catalogos
            });
    

        });


});
 

module.exports = app;