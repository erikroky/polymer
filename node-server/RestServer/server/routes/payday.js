const express = require('express'); 
const _ = require('underscore');
const request = require('request');
const Payday = require('../models/payday');
const Loans = require('../models/loans');
const Account = require('../models/account');
const Movimiento = require('../models/movimiento');
const Usuario = require('../models/usuario');
const nodemailer = require('nodemailer');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

const app = express();
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    
    next();
 });
  
app.get('/payday/:cuenta',verificaToken,(req, res) => {
 
let cuenta_ = req.params.cuenta;
 
   /* let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);
*/
Payday.find({ cuenta: cuenta_ }, ' cuenta letra monto diapago estado fechahora  disponible ')
     /*   .skip(desde)
        .limit(limite)*/
        .exec((err, paydays) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                paydays: paydays
            });
    

        });


});
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

app.post('/prestamos', verificaToken,function(req, res) {

    let body = req.body;

    /*request('http://www.google.com', function (error, response, body) {
    console.log('error:', error); // Print the error if one occurred
    console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    console.log('body:', body); // Print the HTML for the Google homepage.
    });*/
    var nueva_cuenta='0011-0241-96'+getRandomInt(11111111,99999999)+'-'+getRandomInt(11,99);
    let accounts = new Account({
        cuenta: nueva_cuenta,
        doi: body.doi , 
        saldo: body.monto, 
        tipo_cuenta: 'LISTA006' ,
        tipo: body.tipo ,
        moneda: body.moneda 
    });
    
    accounts.save((err, accountsDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        } 
        console.info('Ya registré la cuenta');

    });


    
    var contasa=Number(body.monto)*Number(body.tasa); 
    graarprestamo(body,nueva_cuenta,contasa);
  
    res.json({
        ok: true 
    }); 
    

});

graarprestamo = (body,cuenta,valormonto) => {
    let loans = new Loans({
        cuenta: cuenta ,
        tipomoneda: body.moneda , 
        monto: valormonto, 
        adicionales: body.adicional ,
        meses: body.meses ,
        tasa: body.tasa 
    });
     
    loans.save((err, loansDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

       
    });

    
    var montqo=valormonto/Number(body.meses);
    for(var i = 1; i <= body.meses;i++){
        console.info('Hola soy la cuota '+i+ '--'+valormonto);
        graarcuota(body,cuenta,montqo,i);
     }
     

}


graarcuota = (body,cuenta,montoqo,letra) => {
    let payday = new Payday({
        cuenta: cuenta ,
        letra: letra , 
        monto: montoqo, 
        diapago: body.dia ,
        estado: false  
    });
     
    payday.save((err, payday) => {

        if (err) {
            console.info('Cuota Error'+err); 
        }else {
            console.info('Cuota Ok');
        }

       
    });
 

}


app.get('/letras/:cuenta',verificaToken,(req, res) => {
 
    let _cuenta = req.params.cuenta; 
        Payday.find({ cuenta: _cuenta }, ' estado fechahora cuenta letra monto diapago disponible')
         /*   .skip(desde)
            .limit(limite)*/
            .exec((err, paydays) => {
    
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }
                res.json({
                    ok: true,
                    paydays
                });
        
    
            });
    
    
    });
     


 app.post('/pagarletra', verificaToken,function(req, res) { 
     let body = req.body; 
     //console.log("body: "+body);
     console.log("cuenta_cargo: "+body.cuenta_cargo); 
     let ccargo;
     let cpago;
     Account.find({ cuenta: body.cuenta_cargo }, 'doi cuenta saldo tipo_cuenta tipo moneda')
     .exec((err, cuenta) => {
         if (err) {  return res.status(400).json({  ok: false, err  });} //SI HAY ALGUN ERROR 
         if (cuenta.length==0) { return res.status(400).json({ok: false, err: { message: "Cuenta no encontrada Cargo"}});} // SI NO EXISTE LA CUENTA:
         ccargo=cuenta[0];   
         //BUSCA CUENTA 2
         Account.find({ cuenta: body.cuenta_pago }, 'doi cuenta saldo tipo_cuenta tipo moneda')
         .exec((err, cuenta2) => {
            if (err) {  return res.status(400).json({  ok: false, err  });} //SI HAY ALGUN ERROR 
            if (cuenta2.length==0) { return res.status(400).json({ok: false, err: { message: "Cuenta no encontrada Pago"}});} // SI NO EXISTE LA CUENTA:
            cpago=cuenta2[0];   
        let topoc_;
            if(ccargo!=null && cpago!=null){
                console.info('Cuenta cargo s0: '+  ccargo);
                console.info('Cuenta cargo s1: '+  ccargo.saldo);
                console.info('Cuenta cargo Number(ccargo.saldo): '+ Number(ccargo.saldo));
               // ccargo.saldo=Number(ccargo.saldo) - Number(body.monto);
              //  cpago.saldo=Number(cpago.saldo) - Number(body.monto);
                //ACTUALIZA LAS 2 CUENTA
                var min=Number(body.monto),max=Number(ccargo.saldo);
                console.info('Cuenta cargo min : '+ min);
                console.info('Cuenta pago max: '+max); 
                if(min<=max) {   
                    if(ccargo.moneda==='PEN'&&cpago.moneda==='USD'){
                    topoc_='pen_usd';
                        console.info('PEN ==> USD');
                    }else  if(ccargo.moneda==='USD'&&cpago.moneda==='PEN'){
                        topoc_='usd_pen';
                            console.info('USD ==> PEN'); 
                    }else {
                        console.info('SIN CONVERSION');
                    } 
                    body.cuenta = ccargo.cuenta;
                    body.cuenta_abono=cpago.cuenta;
            actualizarSaldoCuenta(ccargo,1,body,res,topoc_,ccargo,'Pago de Préstamo','PREST001');
            actualizarSaldoCuenta(cpago,0,body,res,topoc_,ccargo,'Pago de Préstamo','PREST001');
            console.info(body.cuenta_pago);
            console.info(body.letra);
             
                   Payday.findOneAndUpdate({ cuenta: body.cuenta_pago, letra:body.letra}, { estado: true, disponible:false}, (err, paydayslast) => { 
                    if (err) {
                        console.info("ERROR");
                        return res.status(400).json({ok: false, err: { message: "Error al actualzar Cuota"}});
                    }   
                    console.info("TERMINA CON: "+paydayslast);
                        res.json({
                            ok: true,
                            paydayslast
                        });  
                });  
            }else {
                return res.status(400).json({
                    ok: false,
                err: { message:  "No cuenta con fondos suficientes."}
                });
            }  
        }else {
            res.json({
                ok: true,
                ccargo: 'no hay cuentas:'+ (ccargo=!null) +'::'+ (cpago!=null)
            });
        }
            });//segunda cuenta
     }); // primera cuenta

     console.info('Temtina busquedas, responde');  
 });

 app.post('/pagarcuota',verificaToken, function(req, res) { 
    let body = req.body; 
    //console.log("body: "+body);
    console.log("cuenta_cargo: "+body.cuenta_cargo); 
    let ccargo;
    let cpago;
    Account.find({ cuenta: body.cuenta_cargo }, 'doi cuenta saldo tipo_cuenta tipo moneda')
    .exec((err, cuenta) => {
        if (err) {  return res.status(400).json({  ok: false, err  });} //SI HAY ALGUN ERROR 
        if (cuenta.length==0) { return res.status(400).json({ok: false, err: { message: "Cuenta no encontrada Cargo"}});} // SI NO EXISTE LA CUENTA:
        ccargo=cuenta[0];   
        //BUSCA CUENTA 2
        Account.find({ cuenta: body.cuenta_pago }, 'doi cuenta saldo tipo_cuenta tipo moneda')
        .exec((err, cuenta2) => {
           if (err) {  return res.status(400).json({  ok: false, err  });} //SI HAY ALGUN ERROR 
           if (cuenta2.length==0) { return res.status(400).json({ok: false, err: { message: "Cuenta no encontrada Pago"}});} // SI NO EXISTE LA CUENTA:
           cpago=cuenta2[0];   
       let topoc_;
           if(ccargo!=null && cpago!=null){
               console.info('Cuenta cargo s0: '+  ccargo);
               console.info('Cuenta cargo s1: '+  ccargo.saldo);
               console.info('Cuenta cargo Number(ccargo.saldo): '+ Number(ccargo.saldo));
              // ccargo.saldo=Number(ccargo.saldo) - Number(body.monto);
             //  cpago.saldo=Number(cpago.saldo) - Number(body.monto);
               //ACTUALIZA LAS 2 CUENTA
               var min=Number(body.monto),max=Number(ccargo.saldo);
               console.info('Cuenta cargo min : '+ min);
               console.info('Cuenta pago max: '+max); 
               if(min<=max) {   
                   if(ccargo.moneda==='PEN'&&cpago.moneda==='USD'){
                   topoc_='pen_usd';
                       console.info('PEN ==> USD');
                   }else  if(ccargo.moneda==='USD'&&cpago.moneda==='PEN'){
                       topoc_='usd_pen';
                           console.info('USD ==> PEN'); 
                   }else {
                       console.info('SIN CONVERSION');
                   } 
                   body.cuenta = ccargo.cuenta;
                   body.cuenta_abono=cpago.cuenta;
           actualizarSaldoCuenta(ccargo,1,body,res,topoc_,ccargo,'Pago de Tajeta','CARD001');
           actualizarSaldoCuenta(cpago,0,body,res,topoc_,ccargo,'Pago de Tarjeta','CARD001');
           console.info(body.cuenta_pago);
           console.info(body.letra);
            
                  Payday.findOneAndUpdate({ cuenta: body.cuenta_pago, letra:body.letra}, { estado: true, disponible:false}, (err, paydayslast) => { 
                   if (err) {
                       console.info("ERROR");
                       return res.status(400).json({ok: false, err: { message: "Error al actualzar Cuota"}});
                   }   
                   console.info("TERMINA CON: "+paydayslast);
                       res.json({
                           ok: true,
                           paydayslast
                       });  
               });  
           }else {
               return res.status(400).json({
                   ok: false,
               err: { message:  "No cuenta con fondos suficientes."}
               });
           }  
       }else {
           res.json({
               ok: true,
               ccargo: 'no hay cuentas:'+ (ccargo=!null) +'::'+ (cpago!=null)
           });
       }
           });//segunda cuenta
    }); // primera cuenta

    console.info('Temtina busquedas, responde');  
});

 actualizarSaldoCuenta = (cuenta,tipo,body,res, opciontc,cuenta2,tipoalt,tipopag)=> {
    var body_monto_tc;//CONVERTIDO AL TIPO DE CAMBIO;
    var compra=3.20; var venta= 3.45;
    var final=false; var logical; var cuenta_operacion; 

    if(tipo===1){ 
        console.info("-CUENTA SALDO FASE 0: "+cuenta.saldo); 
        cuenta.saldo = (parseFloat(cuenta.saldo)- parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
        console.info("-CUENTA SALDO FASE 1: "+cuenta.saldo);
        logical='- '; 
        cuenta_operacion=body.cuenta + ' '+cuenta.moneda; 
        body_monto_tc=body.monto;
    }else if(tipo===0){
        var sadlo=cuenta.saldo;
        console.info("_CUENTA SALDO FASE 0: "+cuenta.saldo); 
        final=true;
        if(opciontc==='pen_usd'){ 
            if(tipoalt === 'Pago de Préstamo'){ 
                cuenta.saldo = parseFloat(cuenta.saldo)-(body.monto/venta); //ACTUALIZAS EL SALDO 
            }else {
                cuenta.saldo = (parseFloat(cuenta.saldo)+ parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
            }
           
            body_monto_tc=parseFloat(body.monto/venta).toFixed(2); 
        }else if(opciontc==='usd_pen'){  

            if(tipoalt === 'Pago de Préstamo'){ 
                cuenta.saldo =  parseFloat(cuenta.saldo)-(body.monto*compra); //ACTUALIZAS EL SALDO 
            }else {
                cuenta.saldo = (parseFloat(cuenta.saldo)+ parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
            }
            
            body_monto_tc=parseFloat(body.monto*compra).toFixed(2);
        }else {
            if(tipoalt === 'Pago de Préstamo'){ 
                cuenta.saldo = (parseFloat(cuenta.saldo)- parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
            }else {
                cuenta.saldo = (parseFloat(cuenta.saldo)+ parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
            } 
            body_monto_tc= body.monto;
        }

        if(tipoalt === 'Pago de Préstamo'){
            cuenta.saldo = (parseFloat(cuenta.saldo)- parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
            }else {
                cuenta.saldo = (parseFloat(sadlo)+ parseFloat(body.monto)); //ACTUALIZAS EL SALDO 
        }

        if(tipoalt === 'Pago de Préstamo'){
            logical='- ';
        }else {
            logical='+ ';
        }
       
        
        cuenta_operacion=body.cuenta_abono+ ' '+cuenta.moneda; 
        console.info("_CUENTA SALDO FASE 1: "+cuenta.saldo);
    } 
    
    var date = new Date();
    var current_hour = (date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ':' + date.getMilliseconds());
    console.info("CUENTA SALDO FASE 2: "+cuenta.saldo);
    cuenta.saldo = parseFloat( cuenta.saldo).toFixed(2);
    console.info("CUENTA SALDO FASE 3: "+cuenta.saldo);
    Account.findByIdAndUpdate(cuenta._id, cuenta, (err, cuentalast) => { 
        if (err) {
            return res.status(400).json({ok: false, err: { message: "Error al actualzar Saldo"+ tipo}});
        } 

        //REGISTRAR MOVIMIENTO:
        var movimiento = new Movimiento({
            cuenta: cuenta_operacion,
            canal: 'B.Internet',
            doi: cuenta.doi,
            operacion: tipoalt,
            tipopago: tipopag,
            monto: logical.concat(parseFloat(body_monto_tc).toFixed(2)) ,
            /*cuenta_abono: body.cuenta_abono,*/
            doi_abono: body.doi,
            fechahora: current_hour
        });
        
       registrarMovieento(movimiento,res,final,body,body_monto_tc,cuentalast,cuenta2);
    })
}
 

registrarMovieento = (movimiento,res,final,body,body_monto_tc,c1,c2 ) => { 

    

    
    movimiento.save((err, movimientoDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err: { message: err}
            });
        }
        /*
        if(final){
            res.json({
                ok: true,
                movimiento: movimientoDB
            });
        }*/

        Usuario.findOne({ doi: movimiento.doi }, (err, usr) => {

            if (err) {
                console.log('usuario error:'+ err);
            }
    
            if (!usr) {
                console.log('usuario no existe');
                   
            }else {
                console.log('usuario  existe:' );
                if(final){
                
                   SendMail(usr.email, usr.nombre, movimiento.operacion, movimiento._id, movimiento.fechahora,c1.cuenta+' '+c1.tipo,c1.moneda +' '+  parseFloat(body.monto).toFixed(2), c2.cuenta+' '+c2.tipo,c2.moneda +' '+ body_monto_tc, usr.nombre);
                }
                
            }
        });
       

       
    });
}

SendMail = (to,titular_,tipo_,numero_,fecha_hora_,cuanta_cargo_,importe_,cuenta_abono_,importe_abono_,titular_abono_) => { 
    var transporter = nodemailer.createTransport({
        host: "hd-4918.banahosting.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
          user: "api-test@latam-server.com",
          pass: "bbva2019!"
        },logger: true,
        debug: true // include SMTP traffic in the logs
      });
      
      var body='<div><div class="adM">&nbsp;</div><center><table width="540px" cellspacing="0"><tbody><tr><td colspan="2" align="left"><img class="CToWUd" style="display: block; margin-left: auto; margin-right: auto;" src="https://s3-eu-west-1.amazonaws.com/rankia/images/valoraciones/0010/9848/Banco-BBVA-Continental.jpg" width="265" height="70" align="left" border="0" data-image-whitelisted=""/></td></tr><tr><td style="font-family: Verdana; color: #2f4181; width: 854px; font-size: 17px;" align="left"><p style="text-align: center;">&nbsp;<strong>Constancia de Operaci&oacute;n</strong></p></td></tr><tr><td colspan="2"><div style="width: 100%;">Tu operaci&oacute;n fue realizada con &eacute;xito.</div></td></tr><tr><td align="left"><table border="0" width="100%"><tbody><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Titular:</td><td align="left">@TITULAR</td></tr><tr><td align="left">Tipo de operaci&oacute;n:</td><td align="left">@TIPO</td></tr><tr><td align="left">N&uacute;mero de operaci&oacute;n:</td><td align="left">@NUMERO</td></tr><tr><td align="left">Fecha y hora:</td><td align="left">@FECHA_HORA</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Cuenta de cargo:</td><td align="left">@CUENTA_CARGO</td></tr><tr><td align="left">Importe transferido:</td><td align="left">@IMPORTE_TRANSFERIDO</td></tr><tr><td align="left">Importe cargado:</td><td align="left">@IMPORTE_CARGADO</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Cuenta de abono:</td><td align="left">@CUENTA_ABONO</td></tr><tr><td align="left">Titular cuenta de abono:</td><td align="left">@TITULAR_ABONO</td></tr><tr><td align="left">Importe abonado:</td><td align="left">@IMPORTE_ABONADO</td></tr><tr><td align="left" width="30%">&nbsp;</td></tr><tr><td align="left">Referencia:</td><td align="left">@REFERENCIA</td></tr><tr><td align="left">&nbsp;</td><td align="left">&nbsp;</td></tr></tbody></table></td></tr><tr><td>&nbsp;Atentamente<div style="color: #2f4181; text-align: left; width: 65%;"><h4>&nbsp;BBVA Continental</h4></div></td></tr></tbody></table></center></div>';
      body=body.replace('@TITULAR',titular_);
      body=body.replace('@TIPO',tipo_);
      body=body.replace('@NUMERO',numero_);
      body=body.replace('@FECHA_HORA',fecha_hora_);
      body=body.replace('@CUENTA_CARGO',cuenta_abono_);
      body=body.replace('@IMPORTE_TRANSFERIDO',importe_);
      body=body.replace('@IMPORTE_CARGADO',importe_);
      body=body.replace('@IMPORTE_ABONADO',importe_abono_);
      body=body.replace('@CUENTA_ABONO',cuanta_cargo_);
      body=body.replace('@TITULAR_ABONO',titular_abono_);
      body=body.replace('@REFERENCIA',''); 
      
      var mailOptions = {
          from: '"BBA API " <api-test@latam-server.com>', // sender address
          to: to, // list of receivers
          subject: 'BBVA Continental - Constancia de Operación de tu Banca Por Internet   ', // Subject line
          //text: 'Hello world?', // plain text body
          html: body // html body
      };

      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        }
      });
}
module.exports = app;