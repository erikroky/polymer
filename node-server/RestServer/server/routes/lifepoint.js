const express = require('express');
const _ = require('underscore');
const nodemailer = require('nodemailer');
const Usuario = require('../models/usuario');
const Account = require('../models/account');
const LifePoint = require('../models/lifepoint');
const Movimiento = require('../models/movimiento');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');
//const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');
const app = express();
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "*");

    next();
});

app.get('/lifepoints/:doi', verificaToken,(req, res) => {
    let doi = req.params.doi; 
    console.log(doi);
    LifePoint.findOne({   doi: doi   } )
        /*   .skip(desde)
           .limit(limite)*/
        .exec((err, lifepoint) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err: { message: err}
                });
            }
            console.log("pasa aqui");
            res.json({
                ok: true,
                lifepoint
            });


        });


});



app.post('/lifepoints',verificaToken, function (req, res) {
   
    let topoc_='NONE';
    let body = req.body;
    var venta= 3.45;
    //VALIDAR PARAEMTROS
    if (body.doi==null    ||body.doi<=0  || body.doi == undefined)   { return res.status(200).json({ok: false, err: { message:  "el doi es requerido"}});}
    if (body.cuenta==null ||body.cuenta<=0  || body.cuenta == undefined){ return res.status(200).json({ok: false, err: { message:  "La cuenta es requerida"}});}
    if (body.monto==null  ||body.monto<=0  || body.monto == undefined) { return res.status(400).json({ok: false, err: { message:  "Ingrese un monto válido "}});}
    if (body.points==null  ||body.monto<=0  || body.points == undefined) { return res.status(400).json({ok: false, err: { message:  "Ingrese puntos válidos "}});}
  
    var c1;
     
    console.info('BODY MONTO: '+parseFloat(body.monto).toFixed(2));
        //CUENTA DESTINO
        
        Account.find({  doi: body.doi,  cuenta: body.cuenta  }, (err, acct) => { 
                if (err) {
                    console.info("ERROR");
                    return res.status(400).json({ok: false, err: { message: "Error al obtener Cuenta"}});
                } 
                c1=acct[0];
                body.cuenta=c1.cuenta+' '+c1.moneda;
               
                console.info("newsaldo;"+new_saldo);
                var new_saldo;

                if(c1.moneda==='USD'){
                    topoc_='pen_usd';
                        console.info('PEN ==> USD'); 
                        new_saldo=Number(c1.saldo)+(Number(body.monto)/venta); 
                        console.info("newsaldo;"+new_saldo);
                }else  
                {
                        console.info('SIN CONVERSION'); 
                        new_saldo=Number(c1.saldo)+Number(body.monto); 
                } 

                  new_saldo=parseFloat(new_saldo).toFixed(2)

                Account.findOneAndUpdate({ cuenta: c1.cuenta},  { saldo: new_saldo} , (err, accupdate) => { 
                        if (err) {
                            console.info("ERROR");
                            return res.status(400).json({ok: false, err: { message: "Error al actualzar Cuenta"}});
                        }    
                    });//TERMINA ACOUNT  
         });//TERMINA LIFEPOINT  
        
        //
        LifePoint.find({  doi: body.doi}, (err, lifep) => { 
            if (err) {
                    console.info("ERROR");
                    return res.status(400).json({ok: false, err: { message: "Error al obtener Puntos"}});
                }    
                var newpints=Number(lifep[0].points)-Number(body.points); 
                console.info("new  points;"+newpints);
                LifePoint.findOneAndUpdate({ doi: body.doi},  { points: newpints} , (err, lifepupdate) => { 
                        if (err) {
                            console.info("ERROR");
                            return res.status(400).json({ok: false, err: { message: "Error al actualzar puntos"}});
                        } 

                        var date = new Date();
                        var current_hour = (date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ':' + date.getMilliseconds());
                    
                        //REGISTRAR MOVIMIENTO:
                        var movimiento = new Movimiento({
                            cuenta: body.cuenta,
                            canal: 'Internet',
                            doi: body.doi,
                            operacion: 'Canje ('+body.points+') Puntos Vida',
                            monto: '+ ' +body.monto+'.00' ,
                            /*cuenta_abono: body.cuenta_abono,*/
                            tipopago: 'CANJE001',
                            doi_abono: body.doi,
                            fechahora: current_hour
                        });   
                            movimiento.save((err, movimientoDB) => { 
                                if (err) {
                                    return res.status(400).json({
                                        ok: false,
                                        err: { message: err}
                                    });
                                } 
                                res.json({
                                    ok: true,
                                    lifepupdate 
                                }); 
                                console.info("Registró movimiento:"+movimientoDB);
                            });
                        
                    });//TERMINA ACOUNT  
                
                    		
                       
         });//TERMINA LIFEPOINT  
});//TERMINA METODO
module.exports = app;