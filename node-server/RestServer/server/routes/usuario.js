const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');

const Usuario = require('../models/usuario');
const { verificaToken, verificaAdmin_Role } = require('../middlewares/autenticacion');

const app = express();
app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "*");
	  res.header("Access-Control-Allow-Methods", "*");
	  
	  next();
	});
app.get('/usuario', verificaToken, (req, res) => {


    let desde = req.query.desde || 0;
    desde = Number(desde);

    let limite = req.query.limite || 5;
    limite = Number(limite);

    Usuario.find({ estado: true }, 'doi nombre email role estado google img')
        .skip(desde)
        .limit(limite)
        .exec((err, usuarios) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Usuario.count({ estado: true }, (err, conteo) => {

                res.json({
                    ok: true,
                    usuarios,
                    cuantos: conteo
                });

            });


        });


});
app.post('/usuario', function(req, res) {
//app.post('/usuario', function(req, res) {
    console.info('*******REGISTOR USUARIOS******');
 console.info(req.body);
    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        doi: body.doi,
        direccion: body.direccion,
        nacimiento: body.nacimiento,
        tel1: body.tel1,
        tel2: body.tel2,
        tel3: body.tel3,
        email: body.email,
        password: body.password,
        role: body.role
    });
    console.info('*************');
    console.info(usuario);
    if (usuario.password) {
        usuario.password = bcrypt.hashSync(usuario.password, 10);
    }

    usuario.save((err, usuarioDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            usuario: usuarioDB
        });


    });


});

app.put('/usuario/:id', verificaToken, function(req, res) {

    let id = req.params.id;
    console.info(req.body);
    let body = _.pick(req.body, [ 'nombre','doi', 'email', 'img', 'role', 'estado', 'direccion','tel1','tel2','tel3','nacimiento']);

    if (body.nombre==null    ||body.nombre<=0  || body.nombre == undefined)   { return res.status(400).json({ok: false, err: { message:  "el nombre es requerido"}});}
    if (body.doi==null    ||body.doi<=0  || body.doi == undefined)   { return res.status(400).json({ok: false, err: { message:  "el doi es requerido"}});}
    if (body.email==null ||body.email<=0  || body.email == undefined){ return res.status(400).json({ok: false, err: { message:  "el email es requerida"}});}
    if (body.direccion==null  ||body.direccion<=0  || body.direccion == undefined) { return res.status(400).json({ok: false, err: { message:  "La direccion es requerida"}});}
    if (body.nacimiento==null||body.nacimiento<=0  || body.nacimiento == undefined) { return res.status(400).json({ok: false, err: { message:  "La fehca de Nacimiento es requerida "}});}

   
    let bodyok = _.pick(req.body, [ 'nombre', 'email', 'img', 'role', 'estado', 'direccion','tel1','tel2','tel3','nacimiento']);
    Usuario.findByIdAndUpdate(id, bodyok, { new: false }, (err, usuarioDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        } 
        console.info('usuario actualizado');

    });

    Usuario.findById(id,(err, usuarioDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }



        res.json({
            ok: true,
            usuario: usuarioDB
        });

    });

});

app.delete('/usuario/:id', [verificaToken, verificaAdmin_Role], function(req, res) {


    let id = req.params.id;

    // Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {

    let cambiaEstado = {
        estado: false
    };

    Usuario.findByIdAndUpdate(id, cambiaEstado, { new: true }, (err, usuarioBorrado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        };

        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            usuario: usuarioBorrado
        });

    });



});



module.exports = app;