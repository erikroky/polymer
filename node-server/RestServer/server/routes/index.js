const express = require('express');

const appw = express();

appw.use(require('./usuario'));
appw.use(require('./lista')); //catalogos:
appw.use(require('./login'));
appw.use(require('./account'));
appw.use(require('./payday'));
appw.use(require('./lifepoint'));
appw.use(require('./settings'));



module.exports = appw;