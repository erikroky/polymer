// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-list-box/vaadin-list-box.js';
import '@vaadin/vaadin-select/vaadin-select.js';
import '@polymer/paper-card/paper-card.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column';
import '@polymer/iron-form/iron-form.js';
import './shared-bootstrap.js';
import {
    MessageUtils,
    StringUtils,
    Navigation,
    WebService,
    WebSesion,
    WebAction,
    Location,
    Message
} from './util/util-constants'; 
class AppOpPayLoans extends PolymerElement {
    static get template() {
        return html `
 
<style include="shared-bootstrap"> 
</style>  

    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">  </app-location> 
    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">  </app-route>   

    <div class="py-10"></div> 

    `;
    }
 
    
    irahome() {
        Navigation().LINK_HOME(this);
    }
    constructor() {
        super();
    }
    ready() {
        super.ready();
        
    } 
    static get properties() {
       
        this.user = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION));
        return {
            formData: {
                type: Object,
                value: {}
            },
            user: {
                value: this.user
            }
            
        };
    } 
}
window.customElements.define('app-payloans', AppOpPayLoans);