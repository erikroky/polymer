// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js'; 
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-card/paper-card.js'; 
import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-icons/vaadin-icons.js';
import './shared-styles.js';
import './shared-bootstrap.js';

// define the element's class element
class AppPublicZone extends PolymerElement {
    static get template() {
        return html `
           
<style include="shared-bootstrap">

.py-10 {
   height: 30px;
} 

.py-80 {
   height: 150px;
} 
.bg-bbva{
                background-color:   #ffffff;
            }
            
            .blue{
              color: var(--lumo-primary-text-color);
            }
</style>
       
   
 <app-location route="{{route}}" url-space-regex="^[[rootPath]]">   </app-location>
 <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"> </app-route>
 
   
 <div class="py-80"></div>
 <main role="main">

 <section class="  text-center bg-bbva" > 
 <div class="album py-5 bg-bbva"   >
   <div class="container bg-bbva"   >
   <h1 class="blue"><b>BBVA Banco Continental</h1>  <div class="py-10"></b></div>
   <p class="lead text-muted">Tarjetas de Crédito: Descubre los programas de beneficios que tenemos para ti. No importa qué cuenta elijas, disfruta de los beneficios de nuestros canales digitales</p>
   <div class="py-10"></div>
     <div class="row"  >
       <div class="col-md-6">
         <div class="card mb-12 box-shadow">
           <img class="card-img-top"  src="https://www.bbvacontinental.pe/fbin/mult/cuentas-de-ahorro-secundario_tcm1105-679845.png" data-holder-rendered="true">
           <div class="card-body">
             <p class="card-text">Si eres cliente del BBVA Contnental por favor Inicia Sesion en nuestra plataforma.</p>
             <div class="d-flex justify-content-between align-items-center">
               <div class="btn-group">
               <vaadin-button id="btnInicio" on-click="iniciarSesion" theme="success primary">Iniciar Sesion</vaadin-button>
               </div>
               <small class="text-muted">9 mins</small>
             </div>
           </div>
         </div>
       </div>
       <div class="col-md-6">
         <div class="card mb-12 box-shadow">
           <img class="card-img-top"  src="https://www.bbvacontinental.pe/fbin/mult/banca-por-internet-secundario_tcm1105-632506.jpg" data-holder-rendered="true"  >
           <div class="card-body">
             <p class="card-text">Regístrate en nuestra plataforma y disfruta de todos los beneficios de la banca por internet.</p>
             <div class="d-flex justify-content-between align-items-center">
               <div class="btn-group">
               <vaadin-button id="btnRegistro" on-click="crearUsuario" theme="primary">Registrar Usuario</vaadin-button> 
               </div>
               <small class="text-muted">9 mins</small>
             </div>
           </div>
         </div>
       </div> 
     </div>
   </div>
 </div>
 </section>
</main>
 
  `;
    }
    // Element class can define custom element reactions
    connectedCallback() {
        super.connectedCallback();
        this.textContent = 'I\'m a custom element! AppHome';
        console.log('connectedCallback: Soy el Home!');
    }

    ready() {
        super.ready();
        console.log('ready: Soy el Home!');
    }

    iniciarSesion() {
      this.set('route.path', '/app-login');
  }

  crearUsuario() {
    this.set('route.path', '/app-register');
}
}



// Associate the new class with an element name
customElements.define('app-publiczone', AppPublicZone);