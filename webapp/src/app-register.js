// Import PolymerElement class

import {  PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js'; 
import '@polymer/paper-button/paper-button.js'; 
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@polymer/paper-card/paper-card.js'; 
import './shared-styles.js';
import './shared-bootstrap.js'; 
import {MessageUtils, Navigation, WebSesion, WebService ,HTTPUtil} from  './util/util-constants.js';



// define the element's class element
class AppRegister extends PolymerElement {
    static get template() {
            return html `   
<iron-ajax id="postRegistroAjax" method="post" content-type="application/json" handle-as="text" on-response="handleUserResponse" on-error="handleUserError">         
</iron-ajax>
 <app-location route="{{route}}" url-space-regex="^[[rootPath]]">   </app-location><app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"> </app-route>
 <div class="py-10"></div>
 <div class="container">
 <div class="header clearfix"></div>
      <vaadin-horizontal-layout>  
                                 
      <form> 
         <div class="center">
         <h2>Registrar Usuario</h2>
         </div>

         <template is="dom-if" if="[[error]]"> 
         <div class="notice notice-danger">
         <strong>Error: </strong> [[error]]
        </div> 
        </template>

        <template is="dom-if" if="[[!error]]"> 
        <div class="notice notice-success">
        <strong>Info: </strong> [[inforpt]]
       </div> 
       </template>
       
        

       <div id="authenticated" hidden$="[[registeredUser.loggedin]]">
         <vaadin-form-layout id="frmRegistro">  
         <vaadin-text-field  id="nombre" label="Nombre Completos" value="{{formData.nombre}}"></vaadin-text-field> 
         <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="doi" placeholder="Documento de Idnetidad" label="Numero de Documento" value="{{formData.doi}}"></vaadin-text-field>
         <vaadin-date-picker label="Birthday" value="{{formData.nacimiento}}"></vaadin-date-picker> 
            
         <vaadin-text-field  id="email" label="Email" value="{{formData.email}}" ></vaadin-text-field>  
         <vaadin-password-field  id="password" placeholder="Enter password" value="{{formData.password}}">
                                   <iron-icon icon="vaadin:password" slot="prefix"></iron-icon>
                                </vaadin-password-field>
         <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="tel1" placeholder="Celular" label="Numero de Teléfono" value="{{formData.tel1}}"></vaadin-text-field>
         <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="tel2" placeholder="Teléfono de Oficina" label="Teléfono de Oficina" value="{{formData.tel2}}"></vaadin-text-field>
         <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="tel3" placeholder="Teléfono de Oficina" label="Teléfono" value="{{formData.tel3}}"></vaadin-text-field>
         <vaadin-text-field  id="direccion" label="Dirección principal" value="{{formData.direccion}}"></vaadin-text-field> 
         <vaadin-form-item>
         <div class="g-recaptcha" data-sitekey="6LfbB4gUAAAAABBOQY02xnN_zL2vpvKCaldZ49ue"></div>
         </vaadin-form-item>
         <vaadin-form-item>
         <vaadin-checkbox   id="dalecheck">Estoy de acuerdo con <a href>Términos &amp; Condiciones</a></vaadin-checkbox>
         </vaadin-form-item>
         <vaadin-form-item>
         <div  class="py-10" id="separator"></div>
         <vaadin-button id="btnlogin" on-click="postRegistro" theme="success primary" class="disable">Registrar</vaadin-button> 
         <vaadin-button id="btnHome" on-click="irahome" theme="contrast primary">Cancelar</vaadin-button> 
         </vaadin-form-item>
         
         <iron-input slot="input" bind-value="{{formData.doi}}">     <input  id="doi_" type="text" value="{{formData.doi}}" ></iron-input> 
         <iron-input slot="input" bind-value="{{formData.nacimiento}}">  <input  id="nacimiento_" type="text" value="{{formData.nacimiento}}" ></iron-input> 
         <iron-input slot="input" bind-value="{{formData.direccion}}">  <input  id="direccion_" type="text" value="{{formData.direccion}}" ></iron-input> 
         <iron-input slot="input" bind-value="{{formData.tel1}}">    <input  id="tel1_" type="text" value="{{formData.tel1}}" ></iron-input> 
         <iron-input slot="input" bind-value="{{formData.tel2}}">    <input  id="tel2_" type="text" value="{{formData.tel2}}" ></iron-input> 
         <iron-input slot="input" bind-value="{{formData.tel3}}">    <input  id="tel3_" type="text" value="{{formData.tel3}}" ></iron-input> 
         <iron-input slot="input" bind-value="{{formData.nombre}}">  <input  id="nombre_" type="text" value="{{formData.nombre}}" ></iron-input>  
         <iron-input slot="input" bind-value="{{formData.email}}">   <input id="email_"   type="text" value="{{formData.email}}"   ><iron-input>
         <iron-input slot="input" bind-value="{{formData.password}}"><input id="password_" class="c-input u-mb-small" type="password" value="{{formData.password}}" placeholder="password"  >
     </iron-input>
         </vaadin-form-layout>
         </div>
      </form> 

      <div class="py-10"></div> 
      </vaadin-horizontal-layout>   

 
   
 </div>

 

 
</div> <!-- /container -->
 
<script src='https://www.google.com/recaptcha/api.js'></script>   
   <style include="shared-bootstrap">
   div {
    font-family: "Roboto", "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}
   .py-10 {
      height: 30px;
   } 
   
   .jumbotron {
       background-color: #e5f0f9 !important;
       border-color: #004b8c  !important;
   }

   .yellow-button {
      text-transform: none;
      color: #eeff41;
    }

    .notice {
        padding: 15px;
        background-color: #fafafa;
        border-left: 6px solid #7f7f84;
        margin-bottom: 10px;
        -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
           -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
    }
    .notice-sm {putUpdate
        padding: 10px;
        font-size: 80%;
    }
    .notice-lg {
        padding: 35px;
        font-size: large;
    }
    .notice-success {
        border-color: #80D651;
    }
    .notice-success>strong {
        color: #80D651;
    }
    .notice-info {
        border-color: #45ABCD;
    }
    .notice-info>strong {
        color: #45ABCD;
    }
    .notice-warning {
        border-color: #FEAF20;
    }
    .notice-warning>strong {
        color: #FEAF20;
    }
    .notice-danger {
        border-color: #d73814;
    }
    .notice-danger>strong {
        color: #d73814;
    }

    .disable {
        pointer-events: none;
        opacity: 0.4;
        opacity: 0.4;
      }
      // Disable scrolling on child elements
      .disable div,
      .disable vaadin-button {
        overflow: hidden;
      }

   </style>
        `;
        } 
     
    ready() {
        super.ready(); 
        this.$.dalecheck.addEventListener('change', e => {
            this._handleClickCheck(e)
        });
    } 
    static get properties() {
      this.error = '';  
      return {
          formData: {
              type: Object,
              value: {}
          }
      }
  }
  postRegistro() {
   
   this.$.postRegistroAjax.url = 'http://localhost:3000/usuario';
   this._setReqBody();
   this.$.postRegistroAjax.generateRequest();
}
_setReqBody() {
   this.$.postRegistroAjax.body = this.formData;
}

_handleClickCheck(e) {
    console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
    if(this.$.dalecheck.checked){
      
        this.$.btnlogin.classList.remove('disable');
    

    }else {
        this.$.btnlogin.classList.add('disable');
    }
}

handleUserResponse(event) {
   console.log('Respnse 0!');
   var response = JSON.parse(event.detail.response);
   console.log('Respnse 1!');
   console.log('Respnse 2!:' + response.usuario);
   if (response.usuario) {
       console.log('Respnse TIENE DATA USUARIO');
       this.error = '';
       this.registeredUser = {
           name: response.usuario.nombre,
           ok: response._id,
           registrado_ok: response.ok
       };
       this.inforpt="Usuario registrado, por favor utilice sus nuevas credenciales para el inicio de sesión";
       // redirect to Secret Quotes
      // this.set('route.path', '/secret-quotes');
      MessageUtils().FRONT_NOTIFICATION(this.inforpt);
       
   }
   console.log('Respnse FINISH!');
   // reset form data
   this.formData = {};
}
handleUserError(event) {
    HTTPUtil().CHECK_ERROR(this,event);
}
irahome() {
    Navigation().LINK_HOME(this);
}
}

 
customElements.define('app-register', AppRegister);