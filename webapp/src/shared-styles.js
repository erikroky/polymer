/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import '@polymer/polymer/polymer-element.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="shared-styles">
  <template>
    <style> 
    :host {
      --lumo-primary-text-color: #003f8c;
    }
      .block {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100 % ;
        #background-color: rgba(0, 0, 0, 0.1);
        border: 10 px solid rgba(0, 0, 0, 0.1);
      }
      app-toolbar{
        height: 80px;
      }
      .container {
        width: 50 % ;
        margin: 0 auto;
        outline: 1 px dotted rgba(0, 0, 0, 0.2);
        overflow: hidden;
      }
      
      vaadin-horizontal-layout { 
        margin: 0 auto;
        align-content: center;
        width: 50% ;
        #background-color: rgba(0, 0, 0, 0.05);
      }  
      vaadin-vertical-layout { 
        margin: 0 auto;
        align-content: center; 
        #background-color: rgba(0, 0, 0, 0.05);
      }  
      paper-tabs {
        /* Toolbar is the main header, so give it some color */
        background-color: #0072c9;
            font-family: 'Roboto', Helvetica, sans-serif;
            color: white;
            --app-toolbar-font-size: 24px;
       } 
             
      .text-tab  { 
                #font-size: 20px;  font-weight: 700;
                color: #FFFFFF;
                text-decoration: none;
      }

      .paper-tab  { 
        font-size: 20px;  font-weight: 700;
    
}
      vaadin-button{
                color: # FFFFFF;
      }
      paper-tabs[no-bar] paper-tab.iron-selected {
        /* color: #ffff8d;*/
        nav paper-tabs paper-tab a.link {
          /* display: block;*/
          text-align: center;
          height: auto!important;
        }
        a: link, a: visited {
          /* background-color: #f44336;*/
          color: white;
          /* padding: 14px 25px;*/
          text-align: center;
          text-decoration: none;
          /* display: inline-block;*/
        }
        a: hover, a: active {
          /* background-color: red;*/
        }
        paper-fab.blue {
          --paper-fab-background: var (--paper-light-blue-500);
          --paper-fab-keyboard-focus-background: var (--paper-light-blue-900);
          width: 32 px;
          height: 32 px;
          padding: 8 px;
          margin-left: 30 px;
        }
        
        
      }

      .card {
        width: 50%;
        margin: 24px;
        padding: 16px;
        color: #757575;
        border-radius: 5px;
        background-color: #fff;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
      }

      .circle {
        display: inline-block;
        width: 64px;
        height: 64px;
        text-align: center;
        color: #555;
        border-radius: 50%;
        background: #ddd;
        font-size: 30px;
        line-height: 64px;
      }

      h1 {
        margin: 16px 0;
        color: #212121;
        font-size: 22px;
      }c
      app-header {
        color: #fff;*/
        background-color: var(--lumo-base-color);
      } 
       
      .center{ 
        margin-top:auto;
        margin-bottom:auto;
        text-align:center;
      
      }  
      
      vaadin-item {
        color: var(--lumo-primary-text-color);
      }
    </style>
  </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content); 