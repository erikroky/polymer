// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js'; 
import '@polymer/paper-button/paper-button.js'; 
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@polymer/paper-card/paper-card.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import './shared-bootstrap.js'

// define the element's class element
class AppRpivateZone extends PolymerElement {
    static get template() {
        return html `
        <style include="shared-bootstrap">
        .block {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            padding: 5px  5px 5px  5px; 
            
        }

        .container {
            width: 100%;
            margin: 0 auto;
            /*outline: 1px dotted rgba(0, 0, 0, 0.2);*/
            overflow: hidden;
        }

        paper-card {
            width: 99%;
            border-radius: 5px;
            border-collapse: separate;
 
        } 
        vaadin-horizontal-layout {
            margin: 0 auto;
            align-content: center;
            width: 70%;
          
            /* background-color: rgba(0, 0, 0, 0.05);*/
        } 
        .py-10 {
            height: 30px;
        } 
        #toast2 {
            --paper-toast-background-color: red;
            --paper-toast-color: white;
            paper-toast {
                width: 300px;
                margin-left: calc(50vw - 150px);
            }
        } 
        .notification {
            font: italic 24pt serif;
            background: red;
            color: #000;
            border: .25em solid #000;
        }
        </style>
<main>
<div id="primary" class="blue4 p-t-b-100 height-full responsive-phone">
<div class="container">
    <div class="row">
        <div class="tab-pane animated fadeInUpShort go active show" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-1-tab">
        <div class="mt-5">
            <h4>
                Support
            </h4>
            <p>For support and help please write your questions in item's comments section on themeforest.</p>
            <hr>
            <h4>
                Docs
            </h4>
             <p>You can find docs in your downloaded zip file or for online version please click following button. <br> Docs related to plugins are included in demo.</p>

            <a href="http://xvelopers.com/wp/themes/docs/xdocs/paper-panel/" target="_blank" class="btn btn-primary ml-auto">Documentation</a>

        </div>
    </div>
    </div>
    </div>
    </div>
    </main>
 
`;
    }
     

    ready() {
        super.ready();
        console.log('ready: Soy el Private ZoneA!');
    }


    static get properties() {
        // this.status = sessionStorage.getItem(document.createElement('app-c').user);
        this.error = '';
        return {
            mydata: {
                type: String,
                value: "Soy el data datattta"
            }

        }; 
    }
}


// Associate the new class with an element name
customElements.define('app-privatezone', AppRpivateZone);