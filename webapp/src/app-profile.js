// Import PolymerElement class
import '@polymer/paper-fab/paper-fab.js';
import '@polymer/paper-button/paper-button.js'; 
import '@polymer/app-route/app-location.js';
import '@polymer/iron-input/iron-input.js'; 
import '@polymer/paper-card/paper-card.js';
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';

import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';

import { WebSesion, Token,HTTPUtil } from './util/util-constants';

// define the element's class element
class AppPofile extends PolymerElement {

    static get template() {
        return html `

        <style> 
            vaadin-horizontal-layout {  
                background-color: #FFFFFF;
              }  

        </style>
        <app-location route="{{route}}"></app-location>
<style include="shared-styles"> 
.disable {
    pointer-events: none;
    opacity: 0.4;
    opacity: 0.4;
  }
  // Disable scrolling on child elements
  .disable div,
  .disable vaadin-button {
    overflow: hidden;
  }
  div {
    font-family: "Roboto", "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}
.notice {
    padding: 15px;
    background-color: #fafafa;
    border-left: 6px solid #7f7f84;
    margin-bottom: 10px;
    -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
       -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
}
.notice-sm {putUpdate
    padding: 10px;
    font-size: 80%;
}
.notice-lg {
    padding: 35px;
    font-size: large;
}
.notice-success {
    border-color: #80D651;
}
.notice-success>strong {
    color: #80D651;
}
.notice-info {
    border-color: #45ABCD;
}
.notice-info>strong {
    color: #45ABCD;
}
.notice-warning {
    border-color: #FEAF20;
}
.notice-warning>strong {
    color: #FEAF20;
}
.notice-danger {
    border-color: #d73814;
}
.notice-danger>strong {
    color: #d73814;
}
</style>  
<app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location> 
      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route> 
      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
<iron-ajax id="updateProfileAjax" 
method="put" 
content-type="application/json" 
handle-as="text"
headers="[[headers]]"
on-response="handleUserResponse"  
on-error="handleUserError">
        </iron-ajax>
      
      
        <div class="py-10"></div>

    
       
<div id="authenticated" > 
            <component-tab></component-tab>
            <vaadin-horizontal-layout>  
                           
            <form> 
            <div class="center">
            <h2>Registrar Usuario</h2>
            </div>
   
            <template is="dom-if" if="[[error]]"> 
            <div class="notice notice-danger">
            <strong>Error: </strong> [[error]]
           </div> 
           </template>
   
           <template is="dom-if" if="[[!error]]"> 
           <div class="notice notice-success">
           <strong>Info: </strong> [[inforpt]]
          </div> 
          </template>
          
           
   
           
            <vaadin-form-layout id="frmRegistro">  
            <vaadin-text-field  id="nombre" label="Nombre Completos" value="{{formData.nombre}}"></vaadin-text-field> 
            <vaadin-text-field  class="disable" prevent-invalid-input pattern="[0-9]*" id="doi" placeholder="Documento de Idnetidad" label="Numero de Documento" value="{{formData.doi}}"></vaadin-text-field>
            <vaadin-date-picker class="disable" label="Birthday" value="{{formData.nacimiento}}"></vaadin-date-picker> 
               
            <vaadin-text-field  class="disable" id="email" label="Email" value="{{formData.email}}" ></vaadin-text-field>  
           
            <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="tel1" placeholder="Celular" label="Numero de Teléfono" value="{{formData.tel1}}"></vaadin-text-field>
            <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="tel2" placeholder="Teléfono de Oficina" label="Teléfono de Oficina" value="{{formData.tel2}}"></vaadin-text-field>
            <vaadin-text-field  prevent-invalid-input pattern="[0-9]*" id="tel3" placeholder="Teléfono de Oficina" label="Teléfono" value="{{formData.tel3}}"></vaadin-text-field>
            <vaadin-text-field  id="direccion" label="Dirección principal" value="{{formData.direccion}}"></vaadin-text-field> 
            <vaadin-form-item>
            <vaadin-checkbox   id="dalecheck">Estoy de acuerdo con <a href>Términos &amp; Condiciones</a></vaadin-checkbox>
            </vaadin-form-item>
            <vaadin-form-item>
            <div  class="py-10" id="separator"></div>
            <vaadin-button id="btnlogin" on-click="putUpdate" theme="success primary" class="disable">Actualizar</vaadin-button> 
            <vaadin-button id="btnHome" on-click="irahome" theme="contrast primary">Cancelar</vaadin-button> 
            </vaadin-form-item>
            
            <iron-input slot="input" bind-value="{{formData.doi}}">     <input  id="doi_" type="text" value="{{formData.doi}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.nacimiento}}">  <input  id="nacimiento_" type="text" value="{{formData.nacimiento}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.direccion}}">  <input  id="direccion_" type="text" value="{{formData.direccion}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.tel1}}">    <input  id="tel1_" type="text" value="{{formData.tel1}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.tel2}}">    <input  id="tel2_" type="text" value="{{formData.tel2}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.tel3}}">    <input  id="tel3_" type="text" value="{{formData.tel3}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.nombre}}">  <input  id="nombre_" type="text" value="{{formData.nombre}}" ></iron-input>  
            <iron-input slot="input" bind-value="{{formData.email}}">   <input id="email_"   type="text" value="{{formData.email}}"   ><iron-input>
            <iron-input slot="input" bind-value="{{formData.password}}"><input id="password_" class="c-input u-mb-small" type="password" value="{{formData.password}}" placeholder="password"  >
        </iron-input>
                            </vaadin-form-layout>
                        </form>
                        </div>
                        
                
                <div class="py-10"></div>
           
        </vaadin-horizontal-layout>       
        
        </div>
       

    `;
    }

    putUpdate() {

        this.$.updateProfileAjax.url = 'http://localhost:3000/usuario/'.concat(this.formData.id);
        this._setReqBody();
        this.$.updateProfileAjax.generateRequest();
    }

    ready() {
        super.ready(); 
        this.$.dalecheck.addEventListener('change', e => {
            this._handleClickCheck(e)
        });
        Token().Set(this.$.updateProfileAjax);
    }

    _handleClickCheck(e) {
        console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
        if(this.$.dalecheck.checked){
          
            this.$.btnlogin.classList.remove('disable');
        
    
        }else {
            this.$.btnlogin.classList.add('disable');
        }
    }

    static get properties() {

        //this.storedUser = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION)); 
        //localStorage.getItem("user-storage", this.storedUser);
        //this.user = 
         
        return {
           

            formData: {
                type: Object,
                value: JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION))
            },
            csrfToken: {
                type: Object,
                value: JSON.parse(sessionStorage.getItem(WebSesion().CODE_TOKEN_SESSION))

            },
            headers: {
                type: Object,
                computed: '_computeHeaders(alg)'
              }

            
        }

    }

    _computeHeaders(alg, typ) {
        var outputHeaders = {};
        
          outputHeaders.token = alg; 
        return outputHeaders;
      }

    _setReqBody() {
        //let datasend = this.formData;
        //let userObject = datasend.toObject();
       // delete this.formData.email;


        this.$.updateProfileAjax.body = this.formData;
    }
    handleUserResponse(event) {
        var response = JSON.parse(event.detail.response);

        if (response.ok) {
            this.error = '';
            this.storedUser = {
                role: response.usuario.role,
                estado: response.usuario.estado,
                doi: response.usuario.doi,
                google: response.usuario.google,
                id: response.usuario._id,
                nombre: response.usuario.nombre,
                email: response.usuario.email,
                nacimiento: response.usuario.nacimiento,
                direccion: response.usuario.direccion,
                tel1: response.usuario.tel1,
                tel2: response.usuario.tel2,
                tel3: response.usuario.tel3,
                loggedin: true //PARA MANTENER LA SESION
            };

            this.inforpt="Información actualizada, para comprobar los cambios por favor inicie sesión nuevamente";

            //DEVULVES EL LA LLAVE DEL USUARIO
            this.formData.key_transactions = this.formData.key_transactions

            this.success_info = 'Informacion Actualizada';
            //this.set('route.path', '/app-profile');
            // reset form data
            // this.formData = {};
            this.successupdate(this.success_info);

            this.storedUserUpdate = {
                role: response.usuario.role,
                estado: response.usuario.estado,
                doi: this.formData.doi,
                google: response.usuario.google,
                id: response.usuario._id,
                nombre: this.formData.nombre,
                email: response.usuario.email,
                nacimiento: this.formData.nacimiento,
                direccion: this.formData.direccion,
                tel1: this.formData.tel1,
                tel2: this.formData.tel2,
                tel3: this.formData.tel3,
                loggedin: true //PARA MANTENER LA SESION
            };
            

            this.$.btnlogin.classList.add('disable');
            this.$.dalecheck.checked=false;

            WebSesion().SAVE_USER_SESSION(this.storedUserUpdate); 
        }


    }

    handleUserError(event) { 
        HTTPUtil().CHECK_ERROR(this,event);
    }

    successupdate(message) {
        var position = 'top-center',
            duration = 3000;
        const msg = message;
        const notify = window.document.createElement('vaadin-notification');
        notify.renderer = function(root) { root.textContent = msg; };
        window.document.body.appendChild(notify);
        notify.id = "notification";
        notify.position = position;
        notify.style = "background-color: red",
            notify.duration = duration;
        notify.opened = true;
        notify.addEventListener('opened-changed', function() {
            window.document.body.removeChild(notify);
        });
    }
}
 
customElements.define('app-profile', AppPofile);