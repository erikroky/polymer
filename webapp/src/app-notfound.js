// Import PolymerElement class
import { PolymerElement } from '@polymer/polymer/polymer-element.js';

// define the element's class element
class AppNotFound extends PolymerElement {

    // Element class can define custom element reactions
    connectedCallback() {
        super.connectedCallback();
        this.textContent = 'I\'m a custom element Not Found!';
        console.log('connectedCallback: Soy el AppNotFound!');
    }

    ready() {
        super.ready();
        console.log('ready: Soy el AppNotFound!');
    }
}


// Associate the new class with an element name
customElements.define('app-notfound', AppNotFound);