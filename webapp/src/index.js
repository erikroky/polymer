import {
    setPassiveTouchGestures,
    setRootPath
} from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-route/app-route.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-fab/paper-fab.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';

import '@vaadin/vaadin-ordered-layout/vaadin-vertical-layout.js';
import '@vaadin/vaadin-item/vaadin-item.js';

import {Navigation, WebSesion, WebService } from  './util/util-constants.js';
import './shared-styles.js'
//import { WebSesion, WebService, Navigation } from './util/util-constants.js';
 
// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);
/**
 * @customElement
 * @polymer
 */
class WebappApp extends PolymerElement {
    static get template() {
        return html `
      
        <style >
        body{
            background: -webkit-linear-gradient(left, #0072ff, #00c6ff);
        }
        .blocki {
            
           
            align-items: center;
            
        
          }

          .center {
            text-align: center;
            /*border: 3px solid green;*/
            color: var(--lumo-body-text-color);
              font-size: var(--lumo-font-size-sm);
              font-family: var(--lumo-font-family);
          }

          :host {
            --lumo-primary-text-color: #003f8c;
          }
            .block {
              display: flex;
              align-items: center;
              justify-content: center;
              width: 100 % ;
              #background-color: rgba(0, 0, 0, 0.1);
              border: 10 px solid rgba(0, 0, 0, 0.1);
            }
            app-toolbar{
              height: 80px;
            }
            .container {
              width: 50 % ;
              margin: 0 auto;
              outline: 1 px dotted rgba(0, 0, 0, 0.2);
              overflow: hidden;
            }
            
            vaadin-horizontal-layout { 
              margin: 0 auto;
              align-content: center;
              width: 50% ;
              #background-color: rgba(0, 0, 0, 0.05);
            }  
            vaadin-vertical-layout { 
              margin: 0 auto;
              align-content: center; 
              #background-color: rgba(0, 0, 0, 0.05);
            }  
            paper-tabs {
              /* Toolbar is the main header, so give it some color */
              background-color: #0072c9;
                  font-family: 'Roboto', Helvetica, sans-serif;
                  color: white;
                  --app-toolbar-font-size: 24px;
             } 
                   
            .text-tab  { 
                      #font-size: 20px;  font-weight: 700;
                      color: #FFFFFF;
                      text-decoration: none;
            }
      
            .paper-tab  { 
              font-size: 20px;  font-weight: 700;
          
      }
            vaadin-button{
                      color: # FFFFFF;
            }
            paper-tabs[no-bar] paper-tab.iron-selected {
              /* color: #ffff8d;*/
              nav paper-tabs paper-tab a.link {
                /* display: block;*/
                text-align: center;
                height: auto!important;
              }
              a: link, a: visited {
                /* background-color: #f44336;*/
                color: white;
                /* padding: 14px 25px;*/
                text-align: center;
                text-decoration: none;
                /* display: inline-block;*/
              }
              a: hover, a: active {
                /* background-color: red;*/
              }
              paper-fab.blue {
                --paper-fab-background: var (--paper-light-blue-500);
                --paper-fab-keyboard-focus-background: var (--paper-light-blue-900);
                width: 32 px;
                height: 32 px;
                padding: 8 px;
                margin-left: 30 px;
              }
              
              
            }
      
            .csard {
              width: 50%;
              margin: 24px;
              padding: 16px;
              color: #757575;
              border-radius: 5px;
              background-color: #fff;
              box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
            }
      
            .circle {
              display: inline-block;
              width: 64px;
              height: 64px;
              text-align: center;
              color: #555;
              border-radius: 50%;
              background: #ddd;
              font-size: 30px;
              line-height: 64px;
            }
      
            h1 {
              margin: 16px 0;
              color: #212121;
              font-size: 22px;
            } 
            app-header {
              color: #fff;*/
              background-color: var(--lumo-base-color);
            } 
             
            .center{ 
              margin-top:auto;
              margin-bottom:auto;
              text-align:center;
            
            }  
            
            vaadin-item {
              color: var(--lumo-primary-text-color);
            }

           

      </style> 
   
      
    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">  </app-location> 
    <app-route route="{{route}}" clas="bg-bbva" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"> </app-route>
      <app-drawer-layout fullbleed="" narrow="{{narrow}}"> 
        <app-header-layout has-scrolling-region="" clas="bg-bbva"> 
          
        <template is="dom-if" if="[[status]]"> 
        <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar> 
          
              <div main-title=""> <img slot="branding" referrerpolicy="no-referrer" src="https://www.bbvacontinental.pe/fbin/mult/logoperu_tcm1105-418187.png" alt="Vaadin Logo" width="30%">
                </div> 
            
               
              <vaadin-vertical-layout   clas="center">  
              <div class="blocki center"> <div clas="center"><vaadin-item><iron-icon icon="vaadin:nurse" style=" font-size: 14px;"></iron-icon>&nbsp; &nbsp;{{status.nombre}} </vaadin-item></div> </div>
              <div class="blocki"> <vaadin-item> <iron-icon icon="vaadin:dollar" style=" font-size: 14px;"></iron-icon>Compra [[compra]] - Venta <iron-icon icon="vaadin:dollar" style=" font-size: 14px;"></iron-icon> [[venta]]</vaadin-item> </div>
               
              </vaadin-vertical-layout>
             
              <vaadin-button  style="height: 100px; font-size: 30px;" on-click="cerrarSesion" raised class="indigo"><iron-icon icon="vaadin:sign-out"></iron-icon></vaadin-button>
              
            </app-toolbar>
            
             
            <paper-tabs id="plain-tabs" selected="0" no-bar sticky  no-slide autoselect autoselect-delay="0"> 
            <!--<paper-tab link="" role="tab" aria-disabled="false" class="x-scope paper-tab-0" tabindex="-1">  
                <div class="tab-content style-scope paper-tab">
                <a  class="text-tab" tabindex="-1" href="[[rootPath]]"> <iron-icon icon="vaadin:home-o"></iron-icon></a>  
                </div> 
            </paper-tab> -->

            <paper-tab link="" role="tab" aria-disabled="false" class="x-scope paper-tab-0" tabindex="-1">  
                <div class="tab-content style-scope paper-tab">
                <a  class="text-tab" tabindex="-1" href="[[rootPath]]app-profile"> <iron-icon icon="vaadin:form"></iron-icon>&nbsp; &nbsp; Mis Datos</a>  
                </div> 
            </paper-tab> 
            <paper-tab link="" role="tab" aria-disabled="false" class="x-scope paper-tab-0" tabindex="-1">  
                <div class="tab-content style-scope paper-tab">
                <a  class="text-tab" tabindex="-1" href="[[rootPath]]app-ops"> <iron-icon icon="vaadin:envelopes"></iron-icon>&nbsp; &nbsp; Canal de Comunicación</a>  
                </div> 
            </paper-tab> 
            <paper-tab link="" role="tab" aria-disabled="false" class="x-scope paper-tab-0" tabindex="-1">  
                <div class="tab-content style-scope paper-tab">
                <a  class="text-tab"   tabindex="-1" href="[[rootPath]]app-operations"> <iron-icon icon="vaadin:info-circle"></iron-icon>&nbsp; &nbsp; Operaciones</a>  
                </div> 
            </paper-tab>
            <paper-tab link="" role="tab" aria-disabled="false" class="x-scope paper-tab-0" tabindex="-1">  
                <div class="tab-content style-scope paper-tab">
                <a   class="text-tab"  tabindex="-1" href="[[rootPath]]app-consultas"> <iron-icon icon="vaadin:search"></iron-icon>&nbsp; &nbsp; Consultas</a>  
                </div> 
            </paper-tab> 
            </paper-tabs>
           

          </app-header>
          </template>
 
          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
         
          <!-- Zona publica -->
          <template is="dom-if" if="[[!status]]"> 
          <app-login name="app-login"></app-login> 
          <app-publiczone name="app-publiczone"></app-publiczone>
          <app-register name="app-register"></app-register>
          </template>
          <!-- Fin de Zona publica --> 

           
            
          
            <!-- Zona privada -->
            <template is="dom-if" if="[[status]]"> 
            <app-profile name="app-profile"></app-profile> 
            <app-privatezone name="app-privatezone"></app-privatezone>
            <app-operations name="app-operations"></app-operations>  
            <app-transfers name="app-transfers"></app-transfers>
            <app-payloans name="app-payloans"></app-payloans>
            <app-cards name="app-cards"></app-cards>
            <app-lifepoints name="app-lifepoints"></app-lifepoints> 
            <app-closeprod name="app-closeprod"></app-closeprod>
            <app-ops name="app-ops"></app-ops>
            </template>
            <!-- Zona Privada-->
          




            <app-notfound name="app-notfound"></app-notfound>  
            
            
            
          </iron-pages>
        </app-header-layout>
        
      </app-drawer-layout>
     
    `;
    }
    static get iniciarSesionFunction() {
        this.$.registerLoginAjax.url = WebService().SERVICIO_LOGIN;
        this.$.registerLoginAjax.body = this.formData;
        this.$.registerLoginAjax.generateRequest();
    }
    static get properties() { 
        return {
            page: {
                type: String,
                reflectToAttribute: true,
                observer: '_pageChanged'
            },
            routeData: Object,
            subroute: Object,
            status: {
                type: String,
                value: JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION)),
                notify: true,
                reflectToAttribute: true
            }, 
            venta : {
                type: String,
                value: '3.45'
            },
            compra : {
                type: String,
                value: '3.20'
            }
        };
    }


    cerrarSesion() {
        sessionStorage.clear();
        this.status = null;
        Navigation().LINK_HOME(this); 
    }
    static get observers() { 
        return [
            '_routePageChanged(routeData.page)'
        ];
    }

    _routePageChanged(page) { 
        this.status = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION)); 
        // Show the corresponding page according to the route.
        //
        // If no page was found in the route data, page will be an empty string.
        // Show 'app-home' in that case. And if the page doesn't exist, show 'view404'.
        if (!page) {
            if(!this.status){
                this.page = Navigation().ALIAS_PUBLIC_ZONE;
            }else {
                this.page = Navigation().ALIAS_PRIVATE_ZONE; 
            } 
        } else if (Navigation().ALIAS_SITES.indexOf(page) !== -1) {
            this.page = page;
        } else if (Navigation().ALIAS_SITES_TEMP.indexOf(page) !== -1) {
            this.page = page;
        } else {
            this.page = Navigation().ALIAS_PUBLIC_NOTFOUND;
        }

        // Close a non-persistent drawer when the page & route are changed.
        /* if (!this.$.drawer.persistent) {
             this.$.drawer.close();
         }
         */
    }

    _pageChanged(page) {
        console.log('_pageChanged(page): Soy '+this.page);
        // Import the page component on demand.
        //
        // Note: `polymer build` doesn't like string concatenation in the import
        // statement, so break it up.
        switch (page) {
            case Navigation().ALIAS_SITES[0]:
                import('./'+Navigation().ALIAS_SITES[0]+'.js');
                break;
            case Navigation().ALIAS_SITES[1]:
                import('./'+Navigation().ALIAS_SITES[1]+'.js');
                break;
            case Navigation().ALIAS_SITES[2]:
                import('./'+Navigation().ALIAS_SITES[2]+'.js');
                break;
            case Navigation().ALIAS_SITES[3]:
                import('./'+Navigation().ALIAS_SITES[3]+'.js');
                break;
            case Navigation().ALIAS_SITES[4]:
                import('./'+Navigation().ALIAS_SITES[4]+'.js');
                break;
            case Navigation().ALIAS_SITES[5]:
                import('./'+Navigation().ALIAS_SITES[5]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[0]:
                import('./'+Navigation().ALIAS_SITES_PATH[0]+'/'+Navigation().ALIAS_SITES_TEMP[0]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[1]:
                import('./'+Navigation().ALIAS_SITES_PATH[0]+'/'+Navigation().ALIAS_SITES_TEMP[1]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[2]:
                import('./'+Navigation().ALIAS_SITES_PATH[0]+'/'+Navigation().ALIAS_SITES_TEMP[2]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[3]:
                import('./'+Navigation().ALIAS_SITES_PATH[0]+'/'+Navigation().ALIAS_SITES_TEMP[3]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[4]:
                import('./'+Navigation().ALIAS_SITES_PATH[0]+'/'+Navigation().ALIAS_SITES_TEMP[4]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[5]:
                import('./'+Navigation().ALIAS_SITES_PATH[0]+'/'+Navigation().ALIAS_SITES_TEMP[5]+'.js');
                break;
            case Navigation().ALIAS_SITES_TEMP[6]:
                import('./'+Navigation().ALIAS_SITES_PATH[1]+'/'+Navigation().ALIAS_SITES_TEMP[6]+'.js');
                break;
            case 'app-notfound':
                import('./app-notfound.js');
                break;
        }
    }


    crearUsuario() {
        this.set('route.path', '/app-register');
    }
    iniciarSesion() {
        this.set('route.path', '/app-login');
    }

    constructor() {
        super();
       
      }

}
window.customElements.define('index-app', WebappApp);