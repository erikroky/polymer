// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-list-box/vaadin-list-box.js';
import '@vaadin/vaadin-select/vaadin-select.js';
import '@polymer/paper-card/paper-card.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column';
import '@polymer/iron-form/iron-form.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-button.js';
import '@vaadin/vaadin-radio-button/vaadin-radio-group.js';
import '../shared-bootstrap.js';
import {
    MessageUtils,
    StringUtils,
    Navigation,
    WebService,
    WebSesion,
    WebAction,
    TipoMovimiento,
    Location,
    Token,
    Message,
    HTTPUtil
} from '../util/util-constants'; 
class AppPuntosVida extends PolymerElement {
    static get template() {
        return html `
 
<style include="shared-bootstrap"> 
 .disable {
            pointer-events: none;
            opacity: 0.4;
            opacity: 0.4;
          }
          // Disable scrolling on child elements
          .disable div,
          .disable textarea {
            overflow: hidden;
          }
</style>  
<form> 
    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">  </app-location> 
    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">  </app-route>   
    <iron-ajax  url="http://localhost:3000/accounts/[[user.doi]]/LISTA001"  handle-as="json" loading="{{cargando}}" id="ajaxCombo1"  on-response="responseCombo1" auto on-error="handleUserError"></iron-ajax>
    <iron-ajax  url="http://localhost:3000/lifepoints/[[user.doi]]"  handle-as="json" loading="{{cargando}}" id="ajaxPuntos"  on-response="responsePuntos" auto on-error="handleUserError"></iron-ajax>
    <iron-ajax id="registrarPago" method="post" content-type="application/json" handle-as="text" on-response="responsePago" on-error="handleUserError"></iron-ajax>
    <div class="py-10"></div> 
    <div class="container">
        <div class="jumbotron">
            <h3 class="display-5">Puntos Vida <iron-icon icon="vaadin:exchange"></iron-icon> Efectivo</h3>Recuerda que 33 puntos vida equivale a S/ 1.00 Sol.
            <p class="lead">Convierte tus Punos Vida a Dinero en efectivo </p>
          
        </div>
        <div class="row marketing"> 
            <div class="col-lg-6"> 
            <iron-input slot="input" bind-value="{{formData.actual}}" style="display: block;">
                <div class="form-group">
                <label for="txtActual">Mis puntos Vida </label>
                <input type="number" pattern="[0-9]*" class="form-control  "  readonly id="txtActual" aria-describedby="emailHelp" placeholder="Cargando...">
                <small id="emailHelp" class="form-text text-muted">Cantidad total de sus puntos vida <span id="info" class="badge badge-secondary  " style="font-size:13px;"> </span></small>
                </div>
            </iron-input>
                <iron-input slot="input" bind-value="{{formData.doi}}" style="display: block;">
                <div class="form-group >
                    <label for="txtCanjear">Puntos a Canjear</label>
                        <div class="input-group">
                             <input type="email" class="form-control disable" id="txtCanjear" type="text" value="{{formData.cantidad}}"  aria-describedby="emailHelp" placeholder="Cantidad">
                            
                             <div class="input-group-append">
                             
                            <span class="input-group-text"  > 
                            <template is="dom-if" if="[[txtPuntosCajear]]"> 
                             <span class="badge badge-success  " style="font-size:13px;"> {{txtPuntosCajear}}</span>
                          </template>
                          &nbsp;<span   id="txtMontoSoles"> S/ 0.00</span>
                            </span>
                            </div>
                        </div>
                    <small id="emailHelp" class="form-text text-muted">Recuerda que 33 puntos vida equivale a S/ 1.00 Sol.</small>
                </div>
                </iron-input>
            </div>
            
            <div class="col-lg-6"> 

            <div class="form-group">
            <label for="txtcbol">Mis cuentas</label>
            <vaadin-combo-box    id="combo1" style="width:100%; " name="combo1"   item-value-path="cuenta"  item-label-path="cuenta">
                    <template>
                    <toggle-button two-line>
                    <div><b>[[item.tipo]]</b> <br> [[item.cuenta]]: ([[item.moneda]] [[item.saldo]])  </div>
                    </paper-item-body>
                    </template>
                </vaadin-combo-box>
            <small id="selecion1" class="form-text text-muted">Cuenta destino de efecivo</small>
            </div>

            <div class="form-group">
            <label for="txtchecking"> </label>
            <vaadin-checkbox   id="dalecheck"  style="width:100%; " >Estoy de acuerdo con <a href>Términos &amp; Condiciones</a></vaadin-checkbox>
             
            </div>

            <div class="form-group">
            <label for="txtbtn"> </label>
            <vaadin-button  id="postCanjebtn" on-click="sendPoints"  on-click="" theme="success primary" class="disable">Canjear</vaadin-button> 
             
            </div> 
            </div>

            <div  class="col-lg-12"> 
            <vaadin-horizontal-layout> 
   <iron-ajax auto
     url="http://localhost:3000/movimiento/[[user.doi]]/[[tipotrans]]"
     method="get"
     id="listamovimientospl"
     handle-as="json"
     loading="{{cargando}}" 
     last-response="{{data}}"
     on-error="handleUserError1">
   </iron-ajax> 
   <vaadin-grid aria-label="Grid Lista" items="{{data.movimientos}}" page-size="3" height-by-rows> 
   
   <vaadin-grid-column width="70px" flex-grow="0"><template class="header">Número</template><template>{{item._id}}</template></vaadin-grid-column>
   <vaadin-grid-column><template class="header">Fecha y Hora de Operacion</template><template>{{item.fechahora}}</template></vaadin-grid-column>
   <vaadin-grid-column   ><template class="header">Canal</template><template>[[item.canal]]</template></vaadin-grid-column>
   <vaadin-grid-column  width="200px"><template class="header">Tipo de Operacion</template><template>[[item.operacion]]</template></vaadin-grid-column> 
   <vaadin-grid-column width="200px"><template class="header">Cuenta</template><template>[[item.cuenta]]</template></vaadin-grid-column> 
   <vaadin-grid-column><template class="header">Moneda y Monto</template><template>[[item.monto]]</template></vaadin-grid-column> 

 </vaadin-grid>

   </vaadin-horizontal-layout> 
            </div>
            
        </div>
    </div>
    <form> 
    `;
    }
 
    responseCombo1(event) { 
        var response =  (event.detail.response); 
       // this.$.combo1.placeholder=response.cuentas.length+' Cuentas existentes';
         this.$.combo1.items =response.cuentas; 
    }

    responsePago(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) { 
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_SUCCESS);
           // Token().Set(this.$.listamovimientospl);
           // this.$.listamovimientospl.generateRequest();
           this.$.combo1.value = null; 
           this.$.txtCanjear.value="";
           this.$.postCanjebtn.classList.add(StringUtils().HTML_DISABLE);
           //this.$.dalecheck.classList.add(StringUtils().HTML_DISABLE); 
           
        
            this.$.txtMontoSoles.textContent='S/ 00.00';
            this.txtPuntosCajear='';


           this.$.dalecheck.checked=false;
           this.formData = {}; 
          
           //Token().Set(this.$.listamovimientospl);
           
           this.$.ajaxCombo1.generateRequest();
           this.$.ajaxPuntos.generateRequest(); 
           Token().Set(this.$.listamovimientospl);
            this.$.listamovimientospl.generateRequest();

            
        }
       
    }
 
    sendPoints() {
        let points = new Object({
            doi: this.user.doi,
            cuenta: this.$.combo1.value,
            monto:  this.montorq,
            points: this.pointsrq 
        });
        this.formData = points; 
        this.$.registrarPago.url = WebService().SERVICIO_PAGO_LIFEPOINTS;
        this.$.registrarPago.body = this.formData;
        this.$.registrarPago.generateRequest(); 
       
    }
 
    

    responsePuntos(event) { 
        var response =  (event.detail.response); 

        this.$.txtActual.value=response.lifepoint.points;
        this.$.puntosvida=Number(this.$.txtActual.value);
        this.$.txtCanjear.classList.remove('disable');
        this.$.info.textContent=response.lifepoint.points;
        
          
    }

    handleUserError(event) {
        HTTPUtil().CHECK_ERROR(this,event);
    }
    irahome() {
        Navigation().LINK_HOME(this);
    }
    constructor() {
        super();
    }
    ready() {
        super.ready();

        this.$.txtCanjear.addEventListener(WebAction().KEYUP, e => {
           
            this._handleKeyUp(e)
        });

        this.$.dalecheck.addEventListener(WebAction().CHANGE, e => {
            this._handleClickCheck(e)
        });
        Token().Set(this.$.listamovimientospl);


        Token().Set(this.$.listamovimientospl);
        Token().Set(this.$.ajaxCombo1);
        Token().Set(this.$.ajaxPuntos);
        Token().Set(this.$.registrarPago);
       // Token().Set(this.$.registrarPago);

       
        
    } 

    _handleKeyUp(e) { 
        if(Number(this.$.txtCanjear.value)<=this.$.puntosvida){
            this.lastval=this.$.txtCanjear.value;
        var numero = this.$.txtCanjear.value ;  
        var resto = numero % this.valorcanje;   
        console.info(this.$.txtCanjear.value);
       console.info('Resto:'+resto);
       console.info('Canjear:'+(this.$.txtCanjear.value-resto));
       var canjear=this.$.txtCanjear.value-resto;
            if ( resto == 0 ){
           // alert("multiplo");
            //this.$.txtMontoSoles.textContent =(this.$.txtCanjear.value)/this.valorcanje + '.00';
            }

            this.txtPuntosCajear= canjear+' Puntos ';
            this.pointsrq=canjear;
            this.montorq=(canjear)/this.valorcanje;
            this.$.txtMontoSoles.textContent =' S/ '+ this.montorq + '.00';

            //QUITAR PUNTOS DE ARRIBA.
         this.$.txtActual.value=this.$.puntosvida-Number(canjear);

        }else if(Number(this.$.txtCanjear.value)>this.$.puntosvida){
            MessageUtils().FRONT_NOTIFICATION('Solo tienes :'+this.$.puntosvida +' Puntos vida'); 
            this.$.txtCanjear.value=this.lastval;
        }
       
    }


    _handleClickCheck(e) {  
 
        
        console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
        if(this.$.dalecheck.checked){
           if (Number(this.$.txtCanjear.value)>=this.valorcanje) { 
                this.$.postCanjebtn.classList.remove(StringUtils().HTML_DISABLE);
            }else if (Number(this.$.txtCanjear.value)<this.valorcanje) { 
                this.$.postCanjebtn.classList.add(StringUtils().HTML_DISABLE);
                MessageUtils().FRONT_NOTIFICATION('Ingrese puntos a canjear');
            this.$.dalecheck.checked=false;  
            } 
            if(Number(this.$.txtCanjear.value)>this.$.puntosvida){
                MessageUtils().FRONT_NOTIFICATION('Sin Trampas!!!: Solo tienes :'+this.$.puntosvida +' Puntos vida'); 
                this.$.txtCanjear.value='';
                this.$.postCanjebtn.classList.add(StringUtils().HTML_DISABLE);
                this.$.dalecheck.checked=false;  
            }

            if( this.$.combo1.value.length<=0){
                MessageUtils().FRONT_NOTIFICATION('Seleccione la cuenta de destino');  
                this.$.postCanjebtn.classList.add(StringUtils().HTML_DISABLE);
                this.$.dalecheck.checked=false; 

            }

        }else {
            this.$.postCanjebtn.classList.add(StringUtils().HTML_DISABLE);
        }
    }
    static get properties() {
       
        this.user = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION));
        
        return {
            formData: {
                type: Object,
                value: {}
            },
            user: {
                value: this.user
            },
            valorcanje: {
                value: 33
            },
            tipotrans: {
                value: [[TipoMovimiento().MOVCAJEPVIDA]]
            }
            
        };
    } 
}
window.customElements.define('app-lifepoints', AppPuntosVida);