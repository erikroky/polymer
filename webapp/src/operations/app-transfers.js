// Import PolymerElement class
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-input/iron-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/paper-fab/paper-fab.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-list-box/vaadin-list-box.js';
import '@vaadin/vaadin-select/vaadin-select.js';
import '@polymer/paper-card/paper-card.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-column';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column';
import '@polymer/iron-form/iron-form.js';
import '../shared-bootstrap.js';
import {
    MessageUtils,
    StringUtils,
    Navigation,
    WebService,
    WebSesion,
    WebAction,
    Location,
    Message,
    TipoMovimiento,
    Token,
    HTTPUtil
} from '../util/util-constants'; 
class AppOpTranfers extends PolymerElement {
    static get template() {
        return html `
 
<style include="shared-bootstrap">
        .block {
            font-family: "Roboto", "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            padding: 5px  5px 5px  5px; 
            
        }

        .container {
            width: 100%;
            margin: 0 auto;
            /*outline: 1px dotted rgba(0, 0, 0, 0.2);*/
            overflow: hidden;
        }

        paper-card {
            width: 99%;
            border-radius: 5px;
            border-collapse: separate;
 
        } 
        vaadin-horizontal-layout {
            margin: 0 auto;
            align-content: center;
            width: 80%;
          
            /* background-color: rgba(0, 0, 0, 0.05);*/
        } 
        .py-10 {
            height: 30px;
        } 
        #toast2 {
            --paper-toast-background-color: red;
            --paper-toast-color: white;
            paper-toast {
                width: 300px;
                margin-left: calc(50vw - 150px);
            }
        } 
        .notification {
            font: italic 24pt serif;
            background: red;
            color: #000;
            border: .25em solid #000;
        }

         .centerdiv {
            text-align: center;
        }

        .disable {
            pointer-events: none;
            opacity: 0.4;
            opacity: 0.4;
          }
          // Disable scrolling on child elements
          .disable div,
          .disable textarea {
            overflow: hidden;
          }
</style>  

    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location> 
      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>  

      <iron-ajax url="http://localhost:3000/lista/LS001" last-response="{{response}}" id="lista1" auto on-error="handleUserError"></iron-ajax> 

      <iron-ajax id="registerLoginAjax1" method="post" content-type="application/json" handle-as="text" on-response="handleUserResponse1" on-error="handleUserError"></iron-ajax>
      
        

        <div class="py-10"></div>
        <vaadin-horizontal-layout> 
        <iron-ajax id="registerLoginAjax" method="get" 
                content-type="application/json" 
                handle-as="text" 
                on-response="handleUserResponse" 
               
                on-error="handleUserError">
                </iron-ajax> 

                <div class="block centerdiv">
                <paper-card >
                <p text-align="center"><h3>Paso1</h3>  </p>
                <p>Selecciona el tipo de Transferencia:  </p>
                <vaadin-combo-box id="combo1" style="width:95%" name="combo1"  items="[[response.catalogos]]" item-value-path="codigo"  item-label-path="nombre">
                
                <template>
                    <toggle-button two-line>
                        <div>[[item.nombre]]</div>
                    </paper-item-body>
                </template>
                </vaadin-combo-box>    
                <p><h5><span  class="badge badge-primary" id="selecion1"></span></h5></p>
                </paper-card>
                </div>

                <div  class="block centerdiv">
                <paper-card  id="p2">
                <p text-align="center"><h3>Paso2</h3>  </p>
               <p> Selecciona la cuenta de cargo:</p>
                <vaadin-combo-box id="combo2" style="width:95%" name="combo2"   item-value-path="cuenta"  item-label-path="cuenta">
                <template>
                <toggle-button two-line>
                <div><b>[[item.tipo]]</b> <br> [[item.cuenta]]:<br> ([[item.moneda]]  <font color="blue"> [[item.saldo]])</font>)  </div>
                </paper-item-body>
             </template>
                </vaadin-combo-box>  
                <p><h5><span  class="badge badge-primary" id="selecion2"></span></h5></p>
                </paper-card>
                </div>
                <div  class="block centerdiv">
                <paper-card  id="p3">
                <p text-align="center"><h3>Paso3</h3>  </p>
               <p> Selecciona el la cuenta de destino: </p>
                <vaadin-combo-box id="combo3" style="width:95%" name="combo3"   item-value-path="cuenta"  item-label-path="cuenta">
                <template>
                  <toggle-button two-line>
                  <div><b>[[item.tipo]]</b> <br> [[item.cuenta]]:<br> ([[item.moneda]] <font color="blue"> [[item.saldo]])</font>  </div>
                  </paper-item-body>
               </template>
                </vaadin-combo-box>  
                <p><h5><span  class="badge badge-primary" id="selecion3"></span></h5></p>
                </paper-card>
                </div>

                <div  class="block centerdiv">
                <paper-card   id="p4">
                <div dir="ltr">
                <vaadin-text-field id="txtmonto" label="Ingresar monto" prevent-invalid-input pattern="[0-9]*[.]*[0-9]*" style="width: 80%;">
                <div slot="prefix" ><span id="currency"></span></div>
                </vaadin-text-field>
                </div>
                 <vaadin-checkbox id="dalecheck" style="padding-left: 20px;"><small>Estoy de acuerdo con los <a href>Términos &amp; Condiciones</a></small></vaadin-checkbox> </p>
               
                 <vaadin-button class="disable" id="btnsuccesstransfer" on-click="sendTransfer"  theme="success primary">Realizar transferencia</vaadin-button>
                </paper-card>
                </div>

               
        </vaadin-horizontal-layout>   
         
        <vaadin-horizontal-layout> 
        <iron-ajax auto
          url="http://localhost:3000/movimiento/[[user.doi]]/[[tipotrans]]"
          method="get"
          id="listamovimientos"
          handle-as="json"
          loading="{{cargando}}" 
          last-response="{{data}}">
        </iron-ajax> 
        <vaadin-grid aria-label="Basic Binding Example" items="{{data.movimientos}}"> 
        
        <vaadin-grid-column width="70px" flex-grow="0"><template class="header">Número Operacion</template><template>{{item._id}}</template></vaadin-grid-column>
        <vaadin-grid-column><template class="header">Fecha y Hora de Operacion</template><template>{{item.fechahora}}</template></vaadin-grid-column>
        <vaadin-grid-column><template class="header">Canal</template><template>[[item.canal]]</template></vaadin-grid-column>
        <vaadin-grid-column><template class="header">Tipo de Operacion</template><template>[[item.operacion]]</template></vaadin-grid-column> 
        <vaadin-grid-column><template class="header">Cuenta</template><template>[[item.cuenta]]</template></vaadin-grid-column> 
        <vaadin-grid-column><template class="header">Moneda y Monto</template><template>[[item.monto]]</template></vaadin-grid-column> 
     
      </vaadin-grid>

        </vaadin-horizontal-layout> 
       

    `;
    }

    handleUserResponse1(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) {
            this.error = StringUtils().EMPTY;
            this.$.combo2.items = [];
            this.$.combo3.items = [];
            this.$.combo1.value = null;
            this.$.combo2.value = null;
            this.$.combo3.value = null;
            this.$.selecion1.textContent='';
            this.$.selecion2.textContent='';
            this.$.selecion3.textContent='';
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
            this.$.dalecheck.checked=false;
            this.$.txtmonto.value='';
            this.$.p2.classList.add(StringUtils().HTML_DISABLE);
            this.$.p3.classList.add(StringUtils().HTML_DISABLE);
            this.$.p4.classList.add(StringUtils().HTML_DISABLE);
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_SUCCESS);
            Token().Set(this.$.listamovimientos);
            this.$.listamovimientos.generateRequest();
            
        }
        this.formData = {};
    }
    sendTransfer() {
        let movimiento = new Object({
            cuenta: this.cuentc1,
            cuenta_abono: this.cuentc2,
            canal: 'Internet',
            doi: this.user.doi,
            monto: this.montocargo,
            //tipo: TipoMovimiento().MOVTRANSFERS,
            operacion: this.tipoopera
        });
        this.formData = movimiento; 
        Token().Set(this.$.registerLoginAjax1);
        this.$.registerLoginAjax1.url = WebService().SERVICIO_MOVIMIENTOS;
        this.$.registerLoginAjax1.body = this.formData;
        this.$.registerLoginAjax1.generateRequest(); 
    }
    irahome() {
        Navigation().LINK_HOME(this);
    }
    constructor() {
        super();
    }
    ready() {
        super.ready();
        this.$.combo1.addEventListener(WebAction().CHANGE, e => {
            this._handleChange(e)
        });
        this.$.combo2.addEventListener(WebAction().CHANGE, e => {
            this._handleChange2(e)
        });
        this.$.combo3.addEventListener(WebAction().CHANGE, e => {
            this._handleChange3(e)
        });
        this.$.txtmonto.addEventListener(WebAction().KEYUP, e => {
            this._handleKeyUp(e)
        });
        this.$.dalecheck.addEventListener(WebAction().CHANGE, e => {
            this._handleClickCheck(e)
        });
        
        this.$.p2.classList.add(StringUtils().HTML_DISABLE);
        this.$.p3.classList.add(StringUtils().HTML_DISABLE);
        this.$.p4.classList.add(StringUtils().HTML_DISABLE);
        
        Token().Set(this.$.listamovimientos);
        Token().Set(this.$.registerLoginAjax1);
        Token().Set(this.$.registerLoginAjax);
        Token().Set(this.$.lista1);
       
    }

    
    _handleClickCheck(e) {
        console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
        if(this.$.dalecheck.checked){
            if(this.$.txtmonto.value>0){ 
            this.$.btnsuccesstransfer.classList.remove(StringUtils().HTML_DISABLE);
        }else if(this.$.txtmonto.value<=0){ 
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_VERIFICARMONTOVALIDO);
            this.$.dalecheck.checked=false;
        }

        }else {
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
        }
    }
    _handleClick(e) {
        console.info(e.target.id + ' was clicked.');
    }
    _handleChange2(e) { 
        this.$.selecion2.textContent='';
       // this.$.selecion3.textContent='';
        const result = this.cuentas_post.filter(cuentacbo => cuentacbo.cuenta == this.$.combo2.value);
        console.log(result);
        console.log(result[0].saldo);
        this.cuentc1 = result[0].cuenta;
        this.monedac1 = result[0].moneda;
        this.saldoc1 = result[0].saldo;
        this.tipo1 = result[0].tipo;
        if (this.monedac1 === 'PEN') {
            this.$.currency.textContent = 'S/ ';
        } else
        if (this.monedac1 === 'USD') {
            this.$.currency.textContent = '$ ';

        } else {
            this.$.currency.textContent = '';
        }
        this.$.selecion2.textContent='Saldo: '+this.monedac1 + ' '+this.saldoc1+ ' - '+this.tipo1;

    }
    _handleChange3(e) { 
        this.$.selecion3.textContent='';
        const result = this.cuentas_post.filter(cuentacbo => cuentacbo.cuenta == this.$.combo3.value);
        console.log(result);
        console.log(result[0].saldo);
        this.cuentc2 = result[0].cuenta;
        this.monedac2 = result[0].moneda;
        this.saldoc2 = result[0].saldo;
        this.tipo2 = result[0].tipo;

        if (this.cuentc1 == this.cuentc2) { 
            MessageUtils().FRONT_NOTIFICATION(Message().MSG_CUENTAS_IGUALES);
            this.$.combo3.value = null;
            this.$.p4.classList.add(StringUtils().HTML_DISABLE);
        } else {
            this.$.p4.classList.remove(StringUtils().HTML_DISABLE);
            this.$.selecion3.textContent='Saldo: '+this.monedac2 + ' '+this.saldoc2+ ' - '+this.tipo2;
        }
      
    }

    _handleChange(e) { 
        this.$.selecion1.textContent='';
        this.$.selecion2.textContent='';
        this.$.selecion3.textContent='';
        this.tipoopera = (this.$.combo1.items.filter(catalogo => catalogo.codigo == this.$.combo1.value))[0].nombre;
        this.$.combo2.items = [];
        this.$.combo3.items = []; 
        this.$.registerLoginAjax.url = WebService().SERVICIO_CUENTAS + this.user.doi + StringUtils().RBAR + this.$.combo1.value;
        this.$.registerLoginAjax.generateRequest();
        this.$.selecion1.textContent='Operacion: '+this.tipoopera;
    }


    _handleKeyUp(e) {
        this.montocargo = this.$.txtmonto.value;

        const formatter = new Intl.NumberFormat(Location().EN, {
            maximumFractionDigits: 2
        });
        if (this.$.txtmonto.value.indexOf(StringUtils().POINT) != -1) {
            this.cantidec = this.$.txtmonto.value.substring(this.$.txtmonto.value.indexOf(StringUtils().POINT), this.$.txtmonto.value.length);
            if (this.cantidec.length > 2) {
                this.$.txtmonto.value = formatter.format(this.$.txtmonto.value);
            }
        }
        var a = parseFloat(this.saldoc1);
        var b = parseFloat(this.$.txtmonto.value);
        if (b <= a) {
           // this.$.btnsuccesstransfer.classList.remove(StringUtils().HTML_DISABLE);
        } else if (b > a) {
            this.$.txtmonto.value = StringUtils().EMPTY;
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
            this.$.dalecheck.checked=false;

            MessageUtils().FRONT_NOTIFICATION(Message().MSG_EXCEDE_MONTO + this.monedac1 + StringUtils().SPACE + this.saldoc1);
        }
        if (this.$.txtmonto.value.toString().length <= 0) {
            this.$.btnsuccesstransfer.classList.add(StringUtils().HTML_DISABLE);
            this.$.dalecheck.checked=false;
        }
    }
    handleUserResponse(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) {
            this.error = StringUtils().EMPTY;
            this.cuentas_post = response.cuentas;
            console.log(response);
            console.log(response.cuentas);
            if (response.cuentas.length != 0) {
                this.$.combo2.items = response.cuentas;
                this.$.combo3.items = response.cuentas;
                this.$.p2.classList.remove(StringUtils().HTML_DISABLE);
                this.$.p3.classList.remove(StringUtils().HTML_DISABLE);
            } else {
                this.$.combo2.value = null;
                this.$.combo3.value = null;
                this.$.p2.classList.add(StringUtils().HTML_DISABLE);
                this.$.p3.classList.add(StringUtils().HTML_DISABLE);
                this.$.p4.classList.add(StringUtils().HTML_DISABLE);
            }
        }
    }
    static get properties() { 
       
        this.user = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION)); 
        return {
            formData: {
                type: Object,
                value: {}
            },
            user: {
                value: this.user
            },
            tipotrans: {
                value: [[TipoMovimiento().MOVTRANSFERS]]
            }
            
        };
        
    }
    handleUserError(event) { 
        HTTPUtil().CHECK_ERROR(this,event);
    }
}
window.customElements.define('app-transfers', AppOpTranfers);