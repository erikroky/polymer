// Import PolymerElement class
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

// define the element's class element
class AppHome extends PolymerElement {
    static get template() {
            return html `
     HOLA SOY EL PUBLIC PUBLC HOME
      <dom-module id="my-form">
    <template> 
      <form is="iron-form" id="form" method="post">
        <paper-input name="name" label="name" id="name"></paper-input>
        <paper-button raised on-click="submitForm">Submit</paper-button>
      </form>
    </template>
    <script type="text/javascript">
         
                alert(this.$.name.value);
              
          
    </script>
</dom-module>
    

    `;
        }
        // Element class can define custom element reactions
    connectedCallback() {
        super.connectedCallback();
        this.textContent = 'I\'m a custom element! AppHome';
        console.log('connectedCallback: Soy el Home!');
    }

    ready() {
        super.ready();
        console.log('ready: Soy el Home!');
    }


}


// Associate the new class with an element name
customElements.define('app-home', AppHome);