import '@polymer/paper-fab/paper-fab.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/iron-ajax/iron-ajax.js';
import {
    PolymerElement,
    html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-button/paper-button.js'; 
import '@polymer/app-route/app-location.js';
import '@polymer/iron-input/iron-input.js'; 
import '@vaadin/vaadin-accordion/vaadin-accordion.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-button/vaadin-button.js'; 
import '@vaadin/vaadin-select/vaadin-select.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-ordered-layout/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-grid/vaadin-grid.js'; 
import '@vaadin/vaadin-grid/vaadin-grid-column';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column';
import '@vaadin/vaadin-details/vaadin-details.js';

import {MessageUtils,
    StringUtils,
    Navigation,
    WebService,
    WebSesion,
    WebAction,
    Location,
    Message,
    TipoMovimiento,
    Token,
    HTTPUtil } from '../util/util-constants';

class AppOps extends PolymerElement {
    static get template() {
        return html ` 
        <app-location route="{{route}}"></app-location>
<style include="shared-styles"> 
.disable {
    pointer-events: none;
    opacity: 0.4;
    opacity: 0.4;
  }
  // Disable scrolling on child elements
  .disable div,
  .disable vaadin-button {
    overflow: hidden;
  }
  div {
    font-family: "Roboto", "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}

vaadin-horizontal-layout {  
                background-color: #FFFFFF;
              }  
 center{ 
        margin-top:auto;
        margin-bottom:auto;
        text-align:center;      
      } 
.notice {
    padding: 15px;
    background-color: #fafafa;
    border-left: 6px solid #7f7f84;
    margin-bottom: 10px;
    -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
       -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
}
.notice-sm { 
    padding: 10px;
    font-size: 80%;
}
.notice-lg {
    padding: 35px;
    font-size: large;
}
.notice-success {
    border-color: #80D651;
}
.notice-success>strong {
    color: #80D651;
}
.notice-info {
    border-color: #45ABCD;
}
.notice-info>strong {
    color: #45ABCD;
}
.notice-warning {
    border-color: #FEAF20;
}
.notice-warning>strong {
    color: #FEAF20;
}
.notice-danger {
    border-color: #d73814;
}
.notice-danger>strong {
    color: #d73814;
}
[part~="cell"] {
        text-align:center;
      }

</style>    
<app-location route="{{route}}" url-space-regex="^[[rootPath]]">
</app-location> 
<app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">     </app-route>
<iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
<iron-ajax url="{{serviciocatalogos}}LS500"  on-response="tiposAjaxResponse" id="lista1" auto on-error="handleUserError" content-type="application/json"  handle-as="text" ></iron-ajax> 
<iron-ajax id="cuentasAjax" method="get"   content-type="application/json"  handle-as="text"  on-response="cuentasAjaxResponse"  on-error="handleUserError"> </iron-ajax> 
<iron-ajax id="opcionesAjax" method="get"    content-type="application/json"  handle-as="text"  on-response="opcionesAjaxResponse"  on-error="handleUserError"> </iron-ajax> 
<iron-ajax id="opcionesRegAjax" method="put"   content-type="application/json"  handle-as="text"  on-response="opcionesRegAjaxResponse"  on-error="handleUserError"> </iron-ajax> 
<div  class="col-lg-12">
   <vaadin-horizontal-layout style=" padding-top: 40px;  padding-bottom: 20px;">
   <vaadin-form-layout id="frmRegistro">  
            <vaadin-text-field readonly="true" id="nombre" label="Nombre Completo" value="{{formData.nombre}}"></vaadin-text-field> 
            <vaadin-text-field readonly="true" prevent-invalid-input pattern="[0-9]*" id="doi" placeholder="Documento de Identidad" label="Numero de Documento" value="{{formData.doi}}"></vaadin-text-field>  
            <vaadin-combo-box  label="Selecione tipo de Contrato" id="combo1" style="width:95%"    item-value-path="subproductos"  item-label-path="nombre">                
                <template>
                    <toggle-button two-line>
                        <div>[[item.nombre]]</div>
                    </paper-item-body>
                </template>
                </vaadin-combo-box> 
                <vaadin-combo-box id="combo2"  label="Seleccione Contrato/Cuenta" style="width:95%" name="combo2"   item-value-path="cuenta"  item-label-path="cuenta">
                <template>
                <toggle-button two-line>
                <div><b>[[item.tipo]]</b> <br> [[item.cuenta]]:  ([[item.moneda]]   )  </div>
                </paper-item-body>
                </template>
                </vaadin-combo-box> 
                <div class="py-10"></div>            
            <iron-input slot="input" bind-value="{{formData.doi}}">     <input  id="doi_" type="text" value="{{formData.doi}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.nacimiento}}">  <input  id="nacimiento_" type="text" value="{{formData.nacimiento}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.direccion}}">  <input  id="direccion_" type="text" value="{{formData.direccion}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.tel1}}">    <input  id="tel1_" type="text" value="{{formData.tel1}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.tel2}}">    <input  id="tel2_" type="text" value="{{formData.tel2}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.tel3}}">    <input  id="tel3_" type="text" value="{{formData.tel3}}" ></iron-input> 
            <iron-input slot="input" bind-value="{{formData.nombre}}">  <input  id="nombre_" type="text" value="{{formData.nombre}}" ></iron-input>  
            <iron-input slot="input" bind-value="{{formData.email}}">   <input id="email_"   type="text" value="{{formData.email}}"   ><iron-input>
            <iron-input slot="input" bind-value="{{formData.password}}"><input id="password_" class="c-input u-mb-small" type="password" value="{{formData.password}}" placeholder="password"  >
        </iron-input>
</vaadin-form-layout>
</vaadin-horizontal-layout>
   <vaadin-horizontal-layout> 
      <vaadin-details>
         <div slot="summary">Agregar Correco electronico</div>
         <vaadin-text-field  id="idnewemail"   style="width: 21em;">
            <div slot="prefix"></div>
         </vaadin-text-field>
         <vaadin-button  on-click="addEmail" theme="primary">+</vaadin-button><hr  style=" border: 1px solid #ffffff;">
         <template is="dom-if" if="[[changemail]]"> 
            <div class="notice notice-success">
           <br> <strong></strong> [[changemail]]
           </div> 
        </template> 
      </vaadin-details>       
   </vaadin-horizontal-layout> 
   <vaadin-horizontal-layout> 
   <template is="dom-if" if="[[viewoptions]]"> 
      <vaadin-grid style="width: 91%;" aria-label="Opciones de envio de informacion" items="{{options}}"   id="griddata" aria-label="Grid Lista"    page-size="3" height-by-rows >
         <vaadin-grid-column  width="100px" text-align="center">
            <template class="header" ><div style="text-align: center"><b>SERVICIO</b><div></template>
            <template>[[item.nombre]]</template>
         </vaadin-grid-column>
         <vaadin-grid-column  width="15px" text-align="center">
            <template class="header"><div style="text-align: center"><b> FÍSICO</b><div></template>
            <template >
               <vaadin-checkbox aria-label="Select Row"  name="nm1[[item.opt]]" id="opt1_[[item.opt]]"  on-change="_isChangedFisico"  checked="[[item.opt1]]"></vaadin-checkbox>
            </template>
         </vaadin-grid-column>
         <vaadin-grid-column   width="15px"  text-align="center">
            <template class="header"><div style="text-align: center"><b> DIGITAL</b><div></template>
            <template>
               <vaadin-checkbox class="select-all"  name="nm2[[item.opt]]"   id="opt2_[[item.opt]]"   on-change="isChangedVirtual"  checked="[[item.opt2]]"></vaadin-checkbox>
           </template>
         </vaadin-grid-column>
         <vaadin-grid-column  width="200px"  text-align="center" >
         <template class="header"><div style="text-align: center"><b>CORREO ELECTRÓNICO</b><div></template>
            <template>
               <vaadin-combo-box class="select-allss" on-change="isChangedEmail" items=[[item.mails]] id="cbo_[[item.opt]]" style="width:100%; " value="[[item.mail]]"   item-value-path="email"  item-label-path="email"> 
               </vaadin-combo-box>
            </template>
         </vaadin-grid-column>
      </vaadin-grid>
      </template> 
   </vaadin-horizontal-layout>

   <vaadin-horizontal-layout>
   <vaadin-form-item>
   <vaadin-checkbox   id="dalecheck">Estoy de acuerdo con <a href>Términos &amp; Condiciones</a></vaadin-checkbox>
   </vaadin-form-item>
   <vaadin-form-item>
   <div  class="py-10" id="separator"></div>
   <vaadin-button id="btnlogin" on-click="putUpdate" theme="success primary" class="disable">Actualizar</vaadin-button> 
  
   </vaadin-form-item>
   </vaadin-horizontal-layout>

</div>
<vaadin-horizontal-layout>
   <div style=" height: 30px;"></div>
</vaadin-horizontal-layout>
<iron-ajax id="ajaxSetCheck" method="put" content-type="application/json"  on-response="responseCheck" on-error="handleUserErronojs"></iron-ajax>
<iron-ajax id="ajaxAddEmail" method="post" content-type="application/json"  on-response="responseEmail" on-error="handleUserErronojs"></iron-ajax>  
<iron-ajax id="updateProfileAjax" method="post"  content-type="application/json"  handle-as="text" headers="[[headers]]" on-response="handleUserResponseSave"   on-error="handleUserError"> </iron-ajax> 
`; 
    }

   
    
    putUpdate() {

        this.$.updateProfileAjax.url = 'http://localhost:3000/finalcuenta/';
        let obj = new Object({
            doi: this.formData.doi,
            cuenta:this.$.combo2.value,
            options:this.options
        }); 
        this._setReqBody();
        this.$.updateProfileAjax.body=obj;
        this.$.updateProfileAjax.generateRequest();
    }
    addEmail() {        
        let email = new Object({
            doi: this.formData.doi,
            email: this.$.idnewemail.value
        });      
     this.$.ajaxAddEmail.url = WebService().SERVICIO_EMAIL; 
     this.$.ajaxAddEmail.body =  email;
     this.$.ajaxAddEmail.generateRequest();  
    }
    
    _isChangedFisico(event) {
        const result = this.catalogos.filter(item => item.subproductos == this.$.combo1.value);       
        let movimiento = new Object({
            doi: this.formData.doi,
            codigo: (event.model.children[1].id).replace('opt1_',''),
            opcion: "1",
            valoropcion: event.model.children[1].checked,
            tipo:  result[0].codigo,
            cuenta:this.$.combo2.value
        });  
        this.$.ajaxSetCheck.url = WebService().SERVICIO_OPCIONESV1; 
        this.$.ajaxSetCheck.body =  movimiento;
        this.$.ajaxSetCheck.generateRequest();      
    }

    isChangedVirtual(event) {
        const result = this.catalogos.filter(item => item.subproductos == this.$.combo1.value);
        let movimiento = new Object({
            doi: this.formData.doi,
            codigo: (event.model.children[1].id).replace('opt2_',''),
            opcion: "2",
            valoropcion: event.model.children[1].checked,
            tipo:  result[0].codigo,
            cuenta:this.$.combo2.value
        });  
        this.$.ajaxSetCheck.url =  WebService().SERVICIO_OPCIONESV1; 
        this.$.ajaxSetCheck.body =  movimiento;
        this.$.ajaxSetCheck.generateRequest();      
        this.$.opcionesAjax.generateRequest(); 
        this.$.opcionesAjax.generateRequest();    
    }

    isChangedEmail(event) {     
        var semail="";
        if(event.target.value.length>0){
        semail= event.target.value;
        } 
        const result = this.catalogos.filter(item => item.subproductos == this.$.combo1.value);         
            let movimiento = new Object({
                doi: this.formData.doi,
                codigo: (event.model.children[1].id).replace('cbo_',''),
                email: semail,
                tipo:result[0].codigo,
                cuenta:this.$.combo2.value
            });  
            this.$.ajaxSetCheck.url = WebService().SERVICIO_ACTUALIZACORREO; 
            this.$.ajaxSetCheck.body =  movimiento;
            this.$.ajaxSetCheck.generateRequest();
            this.$.opcionesAjax.generateRequest();    
            this.$.opcionesAjax.generateRequest();       
    }

    _isChecked(event ) {
       // console.info("es _isChecked");
       // console.info(event);
       // console.info("fin es _isChecked");
     
    } 
  _isChecked(selectAll, indeterminate) { 
    return indeterminate || selectAll;
  }

  _handleClickCheck(e) {
    console.info(e.target.id + ' was _handleClickCheck.'+ this.$.dalecheck.checked);
    if(this.$.dalecheck.checked){
      
        this.$.btnlogin.classList.remove('disable');
    

    }else {
        this.$.btnlogin.classList.add('disable');
    }
}

    ready() {
        super.ready();  
        this.$.combo1.addEventListener(WebAction().CHANGE, e => { 
            this.changemail='';this.$.idnewemail.value=null;this.viewoptions=null;
            this.changevalueCombo1(e)
        });
        this.$.combo2.addEventListener(WebAction().CHANGE, e => { 
            this.changemail='';this.$.idnewemail.value=null;this.viewoptions=null;
            this.steep=2;
            this.changevalueCombo2(e)
        });
        Token().Set(this.$.lista1);
        Token().Set(this.$.cuentasAjax); 
        this.$.lista1.generateRequest(); 

        this.$.dalecheck.addEventListener('change', e => {
            this._handleClickCheck(e)
        });
 
        var label = $('#btnlogin');
        var socket = io('http://localhost:3000');
        console.info('socket');
        socket.on('connect', function(){console.info('connect');});
        socket.on('event', function(data){console.info('event');console.info(data);});
        socket.on('disconnect', function(){console.info('disconnect');}); 
        socket.on('validatemail', function(mensaje) { 
            localStorage.setItem('smail',  Math.floor((Math.random() * 10) + 1));
            callValidateEmail();
        });

        var callValidateEmail = () => { 
            this.smail=JSON.parse(localStorage.getItem('smail')); 
         };
        
     
        
    
    }

   
    cuentasAjaxResponse(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) {
            this.error = StringUtils().EMPTY;
            this.cuentas_post = response.cuentas;
            console.log(response);
            console.log(response.cuentas);
            if (response.cuentas.length != 0) {
                this.$.combo2.items = response.cuentas; 
            } else {
                this.$.combo2.value = null; 
            }
        } 
        // RESPONSE DE OPCIONES
        //this.$.combo2.placeholder=this.$.combo2.items.length+' Cuentas existentes';
        console.info("cuentasss:"+this.$.combo2.items.length);
    }

    opcionesAjaxResponse(event) {
        var response = JSON.parse(event.detail.response);
        if (response.ok) {
            this.viewoptions=true;
            this.error = StringUtils().EMPTY;
            this.options = response.options;
        if (response.next) {
            const valorcombo1 = this.catalogos.filter(item => item.subproductos == this.$.combo1.value); 
            console.info( valorcombo1[0].tiponotificaciones);
            var elementisnews=[]; 
            valorcombo1[0].tiponotificaciones.forEach((element, index) => {  
                var newobj = { 
                    opt: element.codigo,
                    opt1:false,
                    opt2:false,
                    mail:""
                  }
                  elementisnews.push(newobj);
            });
            var dataopciones = new Object({
                doi: this.formData.doi, 
                tipo:  valorcombo1[0].codigo,
                cuenta:this.$.combo2.value,
                opciones:elementisnews
            });              
           this.$.opcionesRegAjax.url = WebService().SERVICIO_OPCIONES; 
             this.$.opcionesRegAjax.body =  dataopciones;
             console.info("a registrar estos:");
             console.info(dataopciones);
           this.$.opcionesRegAjax.generateRequest(); 
            }
        }
    }

    opcionesRegAjaxResponse(event) {
        var response = JSON.parse(event.detail.response);
        console.info(response);
        if (response.ok) {
            const result = this.catalogos.filter(item => item.subproductos == this.$.combo1.value);
            if( this.$.combo2.value.length<=0){
            this.$.opcionesAjax.url = WebService().SERVICIO_OPCIONESV1 + this.user.doi+ StringUtils().RBAR + result[0].codigo+ StringUtils().RBAR +'EMPTY';
            }else{
                this.$.opcionesAjax.url = WebService().SERVICIO_OPCIONESV1 + this.user.doi+ StringUtils().RBAR + result[0].codigo+ StringUtils().RBAR +this.$.combo2.value;
            }
            this.$.opcionesAjax.generateRequest();             
        }
    }
    tiposAjaxResponse(event) {
        var response = JSON.parse(event.detail.response);         
        if (response.ok) {
            this.error = StringUtils().EMPTY;
            this.catalogos = response.catalogos;
            this.$.combo1.items=response.catalogos;            
        }
    }
    changevalueCombo1(e) {  
         this.$.combo2.items = [];
         this.$.combo2.value = null;
         if( this.$.combo1.value.length<=0){
            this.$.cuentasAjax.url = WebService().SERVICIO_CUENTAS + this.user.doi + StringUtils().RBAR + "EMPTYVAL";
          }else {
       this.$.cuentasAjax.url = WebService().SERVICIO_CUENTAS + this.user.doi + StringUtils().RBAR + this.$.combo1.value;}
       this.$.cuentasAjax.generateRequest(); 
      
     }
     changevalueCombo2(e) {  
        
    const result = this.catalogos.filter(item => item.subproductos == this.$.combo1.value);
    if( this.$.combo2.value.length<=0){
        this.$.opcionesAjax.url = WebService().SERVICIO_OPCIONESV1 + this.user.doi+ StringUtils().RBAR + result[0].codigo+ StringUtils().RBAR +'EMPTY';
    }else {
        this.$.opcionesAjax.url = WebService().SERVICIO_OPCIONESV1 + this.user.doi+ StringUtils().RBAR + result[0].codigo+ StringUtils().RBAR +this.$.combo2.value;
    }
    
    this.$.opcionesAjax.generateRequest(); 
    }

    chagneCombo1(event){
        this.msgok='';
    }
    responseCombo1_(event) {  
    }

    responseCombo1(event) {  
    }
    responseCheck(event){  
    }

    responseEmail(event){    
        var response =  (event.detail.response); 
        if(response.ok){
            this.changemail="Para activar su email ( "+ this.$.idnewemail.value + " ) revise su bandeja de entrada y siga los pasos indicados." 
            } 
     }

    _handleClickCheck(e) { 
        if(this.$.dalecheck.checked){          
            this.$.btnlogin.classList.remove('disable');   
        }else {
            this.$.btnlogin.classList.add('disable');
        }
    }

    static get properties() {
 
        this.user = JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION));  
        return {
            serviciocatalogos:{
                value: WebService().SERVICIO_CATALOGOS
            },
            user: {
                value: this.user
            },
            formData: {
                type: Object,
                value: JSON.parse(sessionStorage.getItem(WebSesion().CODE_USER_SESSION))
            },
            csrfToken: {
                type: Object,
                value: JSON.parse(sessionStorage.getItem(WebSesion().CODE_TOKEN_SESSION))

            },
            headers: {
                type: Object,
                computed: '_computeHeaders(alg)'
              }
            ,
            smail: {
                type: String,
                reflectToAttribute: true,
                observer: '_pageChanged' 
            }
        }
    }
  

    _pageChanged(smail) { 
        this.status = JSON.parse(localStorage.getItem('smail')); 
        const result = this.catalogos.filter(item => item.subproductos == this.$.combo1.value);
        this.$.opcionesAjax.url = WebService().SERVICIO_OPCIONESV1 + this.user.doi+ StringUtils().RBAR + result[0].codigo+ StringUtils().RBAR +this.$.combo2.value;
        this.$.opcionesAjax.generateRequest(); 
    }

    _activeChanged(newValue, oldValue) {
        
      }
    _computeHeaders(alg, typ) {
        var outputHeaders = {};
        
          outputHeaders.token = alg; 
        return outputHeaders;
      }

    _setReqBody() { 
        this.$.updateProfileAjax.body = this.formData;
    }

    handleUserResponseSave(event) {
        var response = JSON.parse(event.detail.response);

        if (response.ok) {
            this.error = '';
         
            MessageUtils().FRONT_NOTIFICATION("Acualizado");
            this.$.btnlogin.classList.add('disable');
            this.$.dalecheck.checked=false;
           // this.$.dalecheck.classList.add('disable');
        }
    }
    handleUserResponse(event) {
        var response = JSON.parse(event.detail.response);

        if (response.ok) {
            this.error = '';
            this.storedUser = {
                role: response.usuario.role,
                estado: response.usuario.estado,
                doi: response.usuario.doi,
                google: response.usuario.google,
                id: response.usuario._id,
                nombre: response.usuario.nombre,
                email: response.usuario.email,
                nacimiento: response.usuario.nacimiento,
                direccion: response.usuario.direccion,
                tel1: response.usuario.tel1,
                tel2: response.usuario.tel2,
                tel3: response.usuario.tel3,
                loggedin: true //PARA MANTENER LA SESION
            };

            this.inforpt="Información actualizada, para comprobar los cambios por favor inicie sesión nuevamente";

            //DEVULVES EL LA LLAVE DEL USUARIO
            this.formData.key_transactions = this.formData.key_transactions

            this.success_info = 'Informacion Actualizada';
            //this.set('route.path', '/app-profile');
            // reset form data
            // this.formData = {};
            this.successupdate(this.success_info);

            this.storedUserUpdate = {
                role: response.usuario.role,
                estado: response.usuario.estado,
                doi: this.formData.doi,
                google: response.usuario.google,
                id: response.usuario._id,
                nombre: this.formData.nombre,
                email: response.usuario.email,
                nacimiento: this.formData.nacimiento,
                direccion: this.formData.direccion,
                tel1: this.formData.tel1,
                tel2: this.formData.tel2,
                tel3: this.formData.tel3,
                loggedin: true //PARA MANTENER LA SESION
            };
            

            this.$.btnlogin.classList.add('disable');
            this.$.dalecheck.checked=false;

            WebSesion().SAVE_USER_SESSION(this.storedUserUpdate); 
        }
    }

    handleUserError(event) { 
        HTTPUtil().CHECK_ERROR(this,event);
    }
    handleUserErronojs(event) { 
        HTTPUtil().CHECK_ERRORNOJSON(this,event);
    }
    successupdate(message) {
        var position = 'top-center',
            duration = 3000;
        const msg = message;
        const notify = window.document.createElement('vaadin-notification');
        notify.renderer = function(root) { root.textContent = msg; };
        window.document.body.appendChild(notify);
        notify.id = "notification";
        notify.position = position;
        notify.style = "background-color: red",
            notify.duration = duration;
        notify.opened = true;
        notify.addEventListener('opened-changed', function() {
            window.document.body.removeChild(notify);
        });
    }
}
 
customElements.define('app-ops', AppOps);